﻿namespace MobileCampus.Common.ViewModel
{
    public class EmailTeacherViewModel
    {
        public string Email { get; set; }
        public int IdPersonTeacher { get; set; }
        public int IdCalendar { get; set; }
        public string CalendarKeys { get; set; }

    }
}
