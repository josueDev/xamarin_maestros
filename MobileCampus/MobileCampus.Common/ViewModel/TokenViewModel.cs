﻿using System;

namespace MobileCampus.Common.ViewModel
{
    public class TokenViewModel
    {
        public string Token { get; set; }
        public DateTime Expiration { get; set; }
    }
}
