﻿using System;

namespace MobileCampus.Common.ViewModel
{
    public class SubjectsTeacherViewModel
    {
        public int IdTeacher { get; set; }
        public string IdPeople { get; set; }
        public int IdSubject { get; set; }
        public string Subjects { get; set; }
        //public string CodeSubjects { get; set; }
        //public string Group { get; set; }
        //public string Turn { get; set; }
        //public string Campus { get; set; }
        //public int Cycle { get; set; }
        //public string Subcycle { get; set; }
        //public string Curriculum { get; set; }
        //public string Career { get; set; }
        //public string StartDate { get; set; }
        //public string EndDate { get; set; }
        public string DateSubjects { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int CalendarKey { get; set; }
        public int EventKey { get; set; }
        public string FullNameTeacher { get; set; }
        public string StatusSubjects { get; set; }
    }
}
