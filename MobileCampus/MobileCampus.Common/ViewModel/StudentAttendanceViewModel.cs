﻿namespace MobileCampus.Common.ViewModel
{
    public class StudentAttendanceViewModel
    {
        public int IdStudentMeetingAttendance { get; set; }

        public int PersonId { get; set; }
        public int SectionId { get; set; }

        public string RegisterDate { get; set; }

        public int AttendanceId { get; set; }

        public string Comments { get; set; }

        public int IdAssistanceStudent { get; set; }
    }
}
