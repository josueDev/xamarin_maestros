﻿namespace MobileCampus.Common.ViewModel
{
    public class TeacherViewModel
    {
        public string Id { get; set; }
        public string PeopleId { get; set; }
        public int PersonId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public bool Privacy { get; set; }
    }
}
