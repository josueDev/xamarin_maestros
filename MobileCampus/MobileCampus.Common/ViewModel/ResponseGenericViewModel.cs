﻿namespace MobileCampus.Common.ViewModel
{
    public class ResponseGenericViewModel<T> : ReponseBaseViewModel where T : new()
    {
        public T Data { get; set; }
        public ResponseGenericViewModel(T data) : base()
        {
            Data = data;
        }

        public ResponseGenericViewModel(bool success) : base(success)
        {
            if (!success)
            {
                Data = new T();
            }
        }

        public ResponseGenericViewModel()
        {

        }
    }
}
