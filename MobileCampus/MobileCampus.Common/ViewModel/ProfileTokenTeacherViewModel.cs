﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileCampus.Common.ViewModel
{
    public class ProfileTokenTeacherViewModel
    {
        public TeacherViewModel Profile { get; set; }
        public TokenViewModel Token { get; set; }
    }
}
