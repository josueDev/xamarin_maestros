﻿namespace MobileCampus.Common.ViewModel
{
    public class ReponseBaseViewModel
    {
        public bool Success { get; set; }
        public bool Technical { get; set; } = false;

        public string Message { get; set; }
        public string TechnicalMessage { get; set; }

        public ReponseBaseViewModel()
        {
            Success = true;
            MessageDefault();
        }

        private void MessageDefault()
        {
            Message = "Ok";
        }

        public ReponseBaseViewModel(bool success)
        {
            Success = success;

            if (!Success)
                Message = "Algo salió mal, por favor intenta más tarde.";
            else
                MessageDefault();
        }
    }
}
