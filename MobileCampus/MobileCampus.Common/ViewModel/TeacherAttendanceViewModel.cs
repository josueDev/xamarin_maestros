﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileCampus.Common.ViewModel
{
    public class TeacherAttendanceViewModel
    {
        public int AttendenceId { get; set; }
        public string PeopleCode { get; set; }
        public int SectionId { get; set; }
        public string Status { get; set; }
        public int CalendarKey { get; set; }
    }
}
