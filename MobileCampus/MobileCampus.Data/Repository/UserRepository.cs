﻿using Microsoft.EntityFrameworkCore;
using MobileCampus.Data.Context;
using MobileCampus.Data.Contract;
using MobileCampus.Data.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MobileCampus.Data.Repository
{
    public class UserRepository: IUserRepository
    {
        private readonly CampusContext context;

        public UserRepository(CampusContext context)
        {
            this.context = context;
        }

        
        public async Task<List<StudentList>> GetStudentListAsync(int idPeopleTeacher, string idCalendar)
        {
            var obj = new List<StudentList>();

            try
            {

                var sqlQuery = $"EXEC dbo.sp_student_list_ASIST {idPeopleTeacher}, '{idCalendar}'";

                obj = await context.StudentList.FromSql(sqlQuery).AsNoTracking().ToListAsync();


                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<StudentAssistance>> StudentAttendanceAsync(int personId, int assistance, int sectionId)
        {
            try
            {
                var sqlQuery =
                           $"set dateformat ymd " +
                           $"declare @p1 int  " +
                           "declare @d DATETIME = CAST(GETDATE() AS DATE)" +
                           $"EXEC dbo.spInsStudentMeetingAttendance @StudentMeetingAttendanceId=@p1 output,@SectionId={sectionId},@StudentId={personId}, " +
                           $"@MeetingDate=  @d, " +
                           $"@MeetingAttendanceId={assistance},@Comment=N'APP' " +
                           " select @p1 AS AssistenceId ";

                return await context.StudentAssistances.FromSql(sqlQuery).AsNoTracking().ToListAsync();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<StudentAssistance>> StudentAttendanceUpdateAsync(int IdAssistanceStudent, int assistance)
        {
            try
            {
                var sqlQuery =
                           $"declare @p1 int; " +
                           $"EXEC @p1 = [dbo].[spUpdStudentMeetingAttendance]" +
                           $"@StudentMeetingAttendanceId = {IdAssistanceStudent}," +
                           $"@MeetingAttendanceId = {assistance}," +
                           $"@Comment = N'APP';" +
                           " select @p1 AS AssistenceId ";

                return await context.StudentAssistances.FromSql(sqlQuery).AsNoTracking().ToListAsync();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
