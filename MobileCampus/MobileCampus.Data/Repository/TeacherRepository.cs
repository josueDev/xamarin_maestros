﻿using Microsoft.EntityFrameworkCore;
using MobileCampus.Data.Context;
using MobileCampus.Data.Contract;
using MobileCampus.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileCampus.Data.Repository
{
    public class TeacherRepository : ITeacherRepository
    {
        private readonly CampusContext context;

        public TeacherRepository(CampusContext context)
        {
            this.context = context;
        }

        public async Task<Teacher> GetProfileAsync(string email)
        {
            var lst = new List<Teacher>();

            try
            {

                var sqlQuery = $"EXEC dbo.sp_profile_teacher_ASIST '{email}'";

                lst = await context.Teachers.FromSql(sqlQuery).AsNoTracking().ToListAsync();


                return lst.Any() ? lst[0] : null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<SubjectsTeacher>> GetSubjectTeacherAsync(int idPersonTeacher)
        {
            var obj = new List<SubjectsTeacher>();

            try
            {

                var sqlQuery = $"EXEC dbo.sp_subjects_for_teacher_ASIST {idPersonTeacher}";

                obj = await context.SubjectsTeacher.FromSql(sqlQuery).AsNoTracking().ToListAsync();


                return obj.Any() ? obj : null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<TeacherAssistence> TeacherAttendanceAsync(string peopleCode, int sectionId, string status, int calendarKey)
        {
            var obj = new List<TeacherAssistence>();

            try
            {
                var sqlQuery =  $"EXEC dbo.sp_teacher_attendance_ASIST '{peopleCode}', '{status}', {sectionId}, {calendarKey} ";
                obj = await context.TeacherAssistences.FromSql(sqlQuery).AsNoTracking().ToListAsync();

                return obj.Any() ? obj[0] : null;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> TeacherAttendanceUpdateAsync(int attendenceId,  string status)
        {
            var obj = new List<TeacherAssistence>();

            try
            {
                var sqlQuery = $" UPDATE dbo.ACC_ATTENDANCEDOCENTE_ASIST " +
                               $"SET dbo.ACC_ATTENDANCEDOCENTE_ASIST.ATTENDACE_STATUS = '{status}' "+
                               $"WHERE dbo.ACC_ATTENDANCEDOCENTE_ASIST.ACC_ATTENDANCEDOCENTE = {attendenceId} ";

                return await context.Database.ExecuteSqlCommandAsync(sqlQuery);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> TeacherUpdateNoticePrivacyAsync(string idTeacher)
        {
            var obj = new List<TeacherAssistence>();

            try
            {
                var sqlQuery = $" update AspNetUsers "+
                               $" SET aviso_APP = 1 "+
                               $" WHERE id = '{idTeacher}'";

                return await context.Database.ExecuteSqlCommandAsync(sqlQuery);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Teacher IsTeacher(string email)
        {
            throw new NotImplementedException();
        }
    }
}
