﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MobileCampus.Data.Entity
{
    public class StudentList
    {
        [Key]
        public int IdStuent { get; set; }
        public string NameStudent { get; set; }
        public int CodeAssistance { get; set; }
        public int IdSubject { get; set; }
        //public int Cycle { get; set; }
        //public string Subcycle { get; set; }
        //public string Subjects { get; set; }
        //public string CodeSubjects { get; set; }
        //public string Group { get; set; }
        //public DateTime? StartDate { get; set; }
        //public DateTime? EndDate { get; set; }
        //public DateTime? DateSubjects { get; set; }
        //public string StartTime { get; set; }
        //public string EndTime { get; set; }
        public int CalendarKey { get; set; }
        public int EventKey { get; set; }
        public int tiempo_tolerancia { get; set; }
    }
}
