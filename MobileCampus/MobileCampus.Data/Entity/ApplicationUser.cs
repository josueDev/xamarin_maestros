﻿using Microsoft.AspNetCore.Identity;
using System;

namespace MobileCampus.Data.Entity
{
    public class ApplicationUser : IdentityUser
    {
        public string Student { get; set; }
        public string AlternateEmail { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string FirstLastName { get; set; }
        public string SecondLastName { get; set; }
        public string MatricByCareer { get; set; }
        public DateTime? Created { get; set; }
        public int PersonId { get; set; }
        public string UserType { get; set; }
    }
}
