﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MobileCampus.Data.Entity
{
    [NotMapped]
    public class Teacher
    {
        [Key]
        public string Id { get; set; }
        public string PeopleId { get; set; }
        public string FistName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string LastNamePrefix { get; set; }
        public int PersonId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public bool Privacy{ get; set; }
    }
}
