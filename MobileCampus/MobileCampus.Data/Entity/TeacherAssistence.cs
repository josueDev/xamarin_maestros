﻿using System.ComponentModel.DataAnnotations;

namespace MobileCampus.Data.Entity
{
    public class TeacherAssistence
    {
        [Key]
        public int AssistenceId { get; set; }
    }
}
