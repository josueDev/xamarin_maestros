﻿using System.ComponentModel.DataAnnotations;

namespace MobileCampus.Data.Entity
{
    public class StudentAssistance
    {
        [Key]
        public int AssistenceId { get; set; }
    }
}
