﻿using MobileCampus.Data.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MobileCampus.Data.Contract
{
    public interface IUserRepository
    {
       
        Task<List<StudentList>> GetStudentListAsync(int idPeopleTeacher, string CalendarKeys);
        Task<List<StudentAssistance>> StudentAttendanceAsync(int personId, int assistance, int sectionId);
        Task<List<StudentAssistance>> StudentAttendanceUpdateAsync(int IdAssistanceStudent, int assistance);
    }
}
