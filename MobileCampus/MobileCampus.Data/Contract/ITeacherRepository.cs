﻿using MobileCampus.Data.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MobileCampus.Data.Contract
{
    public interface ITeacherRepository
    {
        Task<Teacher> GetProfileAsync(string email);
        Teacher IsTeacher(string email);
        Task<List<SubjectsTeacher>> GetSubjectTeacherAsync(int idPersonTeacher);
        Task<TeacherAssistence> TeacherAttendanceAsync(string peopleCode, int sectionId, string status, int calendarKey);
        Task<int> TeacherAttendanceUpdateAsync(int attendenceId, string status);
        Task<int> TeacherUpdateNoticePrivacyAsync(string idTeacher);
    }
}
