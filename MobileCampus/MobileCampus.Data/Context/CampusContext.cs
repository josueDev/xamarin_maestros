﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MobileCampus.Data.Entity;

namespace MobileCampus.Data.Context
{
    public class CampusContext: IdentityDbContext<ApplicationUser>
    {
        public virtual DbSet<Teacher> Teachers { get; set; }
        public virtual DbSet<SubjectsTeacher> SubjectsTeacher { get; set; }
        public virtual DbSet<StudentList> StudentList { get; set; }
        public virtual DbSet<StudentAssistance> StudentAssistances { get; set; }
        public virtual DbSet<TeacherAssistence> TeacherAssistences { get; set; }

        public CampusContext(DbContextOptions<CampusContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}
