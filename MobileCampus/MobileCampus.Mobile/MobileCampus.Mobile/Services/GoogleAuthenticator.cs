﻿using MobileCampus.Mobile.Contract;
using MobileCampus.Mobile.Models;
using System;
using Xamarin.Auth;
using Xamarin.Forms;

namespace MobileCampus.Mobile.Services
{
    public class GoogleAuthenticator
    {
        private string AuthorizeUrl = Application.Current.Resources["AuthorizeUrl"].ToString();
        private string AccessTokenUrl = Application.Current.Resources["AccessTokenUrl"].ToString();
        private const bool IsUsingNativeUI = true;

        private OAuth2Authenticator _auth;
        private IGoogleAuthenticationDelegate _authenticationDelegate;

        public delegate void AuthenticationDoneHandler();
        public event AuthenticationDoneHandler AuthenticationDone;

        public GoogleAuthenticator(string clientId, string scope, string redirectUrl, IGoogleAuthenticationDelegate authenticationDelegate)
        {
            _authenticationDelegate = authenticationDelegate;

            _auth = new OAuth2Authenticator(clientId,
                                            string.Empty,
                                            scope,
                                            new Uri(AuthorizeUrl),
                                            new Uri(redirectUrl),
                                            new Uri(AccessTokenUrl),
                                            null,
                                            IsUsingNativeUI);

            _auth.Completed += OnAuthenticationCompleted;
            _auth.Error += OnAuthenticationFailed;
        }

        public OAuth2Authenticator GetAuthenticator()
        {
            return _auth;
        }

        public void OnPageLoading(Uri uri)
        {
            _auth.OnPageLoading(uri);
        }

        private void OnAuthenticationCompleted(object sender, AuthenticatorCompletedEventArgs e)
        {
            if (e.IsAuthenticated)
            {
                var token = new GoogleOAuthToken
                {
                    TokenType = e.Account.Properties["token_type"],
                    AccessToken = e.Account.Properties["access_token"]
                };
                _authenticationDelegate.OnAuthenticationCompleted(token);
            }
            else
            {
                _authenticationDelegate.OnAuthenticationCanceled();
            }

            if (AuthenticationDone != null)
                AuthenticationDone();
        }

        private void OnAuthenticationFailed(object sender, AuthenticatorErrorEventArgs e)
        {
            _authenticationDelegate.OnAuthenticationFailed(e.Message, e.Exception);
            if (AuthenticationDone != null)
                AuthenticationDone();
        }
    }

    public static class GoogleAuthenticatorHelper
    {
        public static GoogleAuthenticator Auth;
    }
}
