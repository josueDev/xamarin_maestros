﻿using GoogleLogin.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MobileCampus.Mobile.Services
{

    public class GoogleAccountInfoService
    {
        public async Task<GoogleProfile> GetProfileAsync(string tokenType, string accessToken)
        {
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);
            var json = await httpClient.GetStringAsync(Application.Current.Resources["ProfileGoogle"].ToString());
            var obj = JsonConvert.DeserializeObject<GoogleProfile>(json);
            return obj;
        }
    }
}
