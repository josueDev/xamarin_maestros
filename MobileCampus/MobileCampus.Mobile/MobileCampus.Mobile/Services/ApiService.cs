﻿using MobileCampus.Mobile.Contract;
using MobileCampus.Mobile.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MobileCampus.Mobile.Services
{
    public class ApiService : IApiService
    {
      
        public async Task<ResponseGeneric<List<StudentList>>> GetStudentList(string urlBase, string urlComplement, int idPersonTeacher, string idCalendar, string tokenType, string accessToken)
        {
            try
            {
                var request = JsonConvert.SerializeObject(new EmailTeacher { IdPersonTeacher = idPersonTeacher, CalendarKeys = idCalendar  }); // IdCalendar = idCalendar
                var content = new StringContent(request, Encoding.UTF8, "application/json");
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };


                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlComplement}";
                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new ResponseGeneric<List<StudentList>> (false)
                    {
                        Success = false,
                        Message = result,
                    };
                }

                var obj = JsonConvert.DeserializeObject<ResponseGeneric<List<StudentList >>> (result);
                return obj;
            }
            catch (Exception ex)
            {
                return new ResponseGeneric<List<StudentList>>(false)
                {
                    Success = false,
                    Message = ex.Message,
                };
            }
        }

        public async Task<ResponseGeneric<List<SubjectsTeacher>>> GetSubjectTeacher(string urlBase, string urlComplement, int idPersonTeacher, string tokenType, string accessToken)
        {
            try
            {
                var request = JsonConvert.SerializeObject(new EmailTeacher { IdPersonTeacher = idPersonTeacher });
                var content = new StringContent(request, Encoding.UTF8, "application/json");
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };


                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"{urlComplement}";
                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new ResponseGeneric<List<SubjectsTeacher>>(false)
                    {
                        Success = false,
                        Message = result,
                    };
                }

                var obj = JsonConvert.DeserializeObject<ResponseGeneric<List<SubjectsTeacher>>>(result);
                return obj;
            }
            catch (Exception ex)
            {
                return new ResponseGeneric<List<SubjectsTeacher>>(false)
                {
                    Success = false,
                    Message = ex.Message,
                };
            }
        }

        public async Task<ResponseGeneric<bool>> IsTeacherAsync(string urlBase, string urlComplement, string email)
        {
            try
            {
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlComplement}?email={email}";
                var response = await client.GetAsync(url);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new ResponseGeneric<bool>(false)
                    {
                        Success = false,
                        Message = result,
                    };
                }

                var obj = JsonConvert.DeserializeObject<ResponseGeneric<bool>>(result);
                return obj;
            }
            catch (Exception ex)
            {
                return new ResponseGeneric<bool>(false)
                {
                    Success = false,
                    Message = ex.Message,
                };
            }
        }

        public async Task<ResponseGeneric<ProfileTeacherToken>> ProfileTokenTeacherAsync(string urlBase, string urlComplement, string email)
        {
            try
            {
                var request = JsonConvert.SerializeObject(new EmailTeacher { Email = email });
                var content = new StringContent(request, Encoding.UTF8, "application/json");

                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"{urlComplement}";
                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new ResponseGeneric<ProfileTeacherToken>(false)
                    {
                        Success = false,
                        Message = result,
                    };
                }

                var obj = JsonConvert.DeserializeObject<ResponseGeneric<ProfileTeacherToken>>(result);
                return obj;
            }
            catch (Exception ex)
            {
                return new ResponseGeneric<ProfileTeacherToken>(false)
                {
                    Success = false,
                    Message = ex.Message,
                };
            }
        }

        public async Task<ResponseGeneric<List<ResponseAssistance>>> RegisterAssistenceStudent(string urlBase, string urlComplement, List<StudentAttendance> studentAttendances, string tokenType, string accessToken)
        {
            try
            {
                var request = JsonConvert.SerializeObject(studentAttendances);
                var content = new StringContent(request, Encoding.UTF8, "application/json");
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase),
                    Timeout = TimeSpan.FromMinutes(15)
                };


                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);


                var url = $"{urlComplement}";
                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new ResponseGeneric<List<ResponseAssistance>> (false)
                    {
                        Success = false,
                        Message = result,
                    };
                }

                var obj = JsonConvert.DeserializeObject<ResponseGeneric<List<ResponseAssistance>>>(result);
                return obj;
            }
            catch (Exception ex)
            {
                return new ResponseGeneric<List<ResponseAssistance>>(false)
                {
                    Success = false,
                    Message = ex.Message,
                };
            }
        }

        public async Task<ResponseGeneric<List<ResponseAssistance>>> UpdateAssistenceStudent(string urlBase, string urlComplement, List<StudentAttendance> studentAttendances, string tokenType, string accessToken)
        {
            try
            {
                var request = JsonConvert.SerializeObject(studentAttendances);
                var content = new StringContent(request, Encoding.UTF8, "application/json");
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase),
                    Timeout = TimeSpan.FromMinutes(15)
                };


                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);


                var url = $"{urlComplement}";
                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new ResponseGeneric<List<ResponseAssistance>>(false)
                    {
                        Success = false,
                        Message = result,
                    };
                }

                var obj = JsonConvert.DeserializeObject<ResponseGeneric<List<ResponseAssistance>>>(result);
                return obj;
            }
            catch (Exception ex)
            {
                return new ResponseGeneric<List<ResponseAssistance>>(false)
                {
                    Success = false,
                    Message = ex.Message,
                };
            }
        }

        public async Task<ResponseGeneric<ResponseAssistance>> RegisterAssistenceTeacher(string urlBase, string urlComplement, TeacherAttendance teacherAttendances, string tokenType, string accessToken)
        {
            try
            {
                var request = JsonConvert.SerializeObject(teacherAttendances);
                var content = new StringContent(request, Encoding.UTF8, "application/json");
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };


                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);


                var url = $"{urlComplement}";
                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new ResponseGeneric<ResponseAssistance>(true)
                    {
                        Success = false,
                        Message = result,
                    };
                }

                var obj = JsonConvert.DeserializeObject<ResponseGeneric<ResponseAssistance>>(result);
                return obj;
            }
            catch (Exception ex)
            {
                return new ResponseGeneric<ResponseAssistance>(false)
                {
                    Success = false,
                    Message = ex.Message,
                };
            }
        }

        public async Task<ResponseGeneric<bool>> UpdateAssistenceTeacher(string urlBase, string urlComplement, TeacherAttendance teacherAttendances, string tokenType, string accessToken)
        {
            try
            {
                var request = JsonConvert.SerializeObject(teacherAttendances);
                var content = new StringContent(request, Encoding.UTF8, "application/json");
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };


                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);


                var url = $"{urlComplement}";
                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new ResponseGeneric<bool>(false)
                    {
                        Success = false,
                        Message = result,
                    };
                }

                var obj = JsonConvert.DeserializeObject<ResponseGeneric<bool>>(result);
                return obj;
            }
            catch (Exception ex)
            {
                return new ResponseGeneric<bool>(false)
                {
                    Success = false,
                    Message = ex.Message,
                };
            }
        }

        public async Task<ResponseGeneric<bool>> UpdateNoticePrivacyTeacher(string urlBase, string urlComplement, Teacher teacher, string tokenType, string accessToken)
        {
            try
            {
                var request = JsonConvert.SerializeObject(teacher);
                var content = new StringContent(request, Encoding.UTF8, "application/json");
                var client = new HttpClient
                {
                    BaseAddress = new Uri(urlBase)
                };


                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);


                var url = $"{urlComplement}";
                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new ResponseGeneric<bool>(false)
                    {
                        Success = false,
                        Message = result,
                    };
                }

                var obj = JsonConvert.DeserializeObject<ResponseGeneric<bool>>(result);
                return obj;
            }
            catch (Exception ex)
            {
                return new ResponseGeneric<bool>(false)
                {
                    Success = false,
                    Message = ex.Message,
                };
            }
        }
    }
}
