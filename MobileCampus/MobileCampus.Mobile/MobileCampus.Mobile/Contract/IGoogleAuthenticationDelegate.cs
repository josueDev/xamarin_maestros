﻿using MobileCampus.Mobile.Models;
using System;

namespace MobileCampus.Mobile.Contract
{
    public interface IGoogleAuthenticationDelegate
    {
        void OnAuthenticationCompleted(GoogleOAuthToken token);
        void OnAuthenticationFailed(string message, Exception exception);
        void OnAuthenticationCanceled();
    }
}
