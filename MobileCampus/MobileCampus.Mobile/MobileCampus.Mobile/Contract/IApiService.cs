﻿using MobileCampus.Mobile.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MobileCampus.Mobile.Contract
{
    public interface IApiService
    {
        Task<ResponseGeneric<bool>> IsTeacherAsync(string urlBase, string urlComplement, string email);
        Task<ResponseGeneric<ProfileTeacherToken>> ProfileTokenTeacherAsync(string urlBase, string urlComplement, string email);
        Task<ResponseGeneric<List<SubjectsTeacher>>> GetSubjectTeacher(string urlBase, string urlComplement, int idPersonTeacher, string tokenType, string accessToken);
        Task<ResponseGeneric<List<StudentList>>> GetStudentList(string urlBase, string urlComplement, int idPersonTeacher, string idCalendar, string tokenType, string accessToken);
        Task<ResponseGeneric<List<ResponseAssistance>>> RegisterAssistenceStudent(string urlBase, string urlComplement, List<StudentAttendance> studentAttendances, string tokenType, string accessToken);
        Task<ResponseGeneric<ResponseAssistance>> RegisterAssistenceTeacher(string urlBase, string urlComplement, TeacherAttendance teacherAttendances, string tokenType, string accessToken);
        Task<ResponseGeneric<bool>> UpdateAssistenceTeacher(string urlBase, string urlComplement, TeacherAttendance teacherAttendances, string tokenType, string accessToken);
        Task<ResponseGeneric<List<ResponseAssistance>>> UpdateAssistenceStudent(string urlBase, string urlComplement, List<StudentAttendance> studentAttendances, string tokenType, string accessToken);
        Task<ResponseGeneric<bool>> UpdateNoticePrivacyTeacher(string urlBase, string urlComplement, Teacher teacher, string tokenType, string accessToken);
    }
}
