﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileCampus.Mobile.Models
{
    public class Menu
    {
        public int Id { get; set; }
        public string Icon { get; set; }
        public string Title { get; set; }
        public string PageName { get; set; }
    }
}
