﻿namespace MobileCampus.Mobile.Models
{
    public class EmailTeacher
    {
        public string Email { get; set; }
        public int IdPersonTeacher { get; set; }
        public int IdCalendar { get; set; }

        public string CalendarKeys { get; set; }
    }
}
