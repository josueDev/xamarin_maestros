﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileCampus.Mobile.Models
{
    public class ProfileTeacherToken
    {
        public Teacher Profile { get; set; }
        public TokenModel Token { get; set; }
    }
}
