﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileCampus.Mobile.Models
{
    public class StudentAttendance
    {
        public int PersonId { get; set; }
        public int SectionId { get; set; }
        public int AttendanceId { get; set; }
        public int IdAssistanceStudent { get; set; }
    }
}
