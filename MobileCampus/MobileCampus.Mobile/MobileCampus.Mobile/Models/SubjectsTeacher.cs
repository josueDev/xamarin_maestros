﻿namespace MobileCampus.Mobile.Models
{
    public class SubjectsTeacher
    {
        public int IdTeacher { get; set; }
        public string IdPeople { get; set; }
        public int IdSubject { get; set; }
        public string Subjects { get; set; }
        public int CalendarKey { get; set; }
        public int EventKey { get; set; }
        public string FullNameTeacher { get; set; }
        public string StatusSubjects { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string DateSubjects { get; set; }
    }
}
