﻿using Newtonsoft.Json;
using System;

namespace GoogleLogin.Models
{
    public class GoogleProfile
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("verified_email")]
        public bool VerifiedEmail { get; set; }

        [JsonProperty("picture")]
        public Uri Picture { get; set; }

        [JsonProperty("hd")]
        public string Hd { get; set; }
    }
}