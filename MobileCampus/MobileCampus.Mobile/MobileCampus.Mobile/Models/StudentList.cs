﻿namespace MobileCampus.Mobile.Models
{
    public class StudentList
    {
        public int IdStuent { get; set; }
        public string NameStudent { get; set; }
        public int CodeAssistance { get; set; }
        public int IdSubject { get; set; }
        public int CalendarKey { get; set; }
        public int EventKey { get; set; }
        public int ToleranceTime { get; set; }
    }
}
