﻿namespace MobileCampus.Mobile.Models
{
    public class TeacherAttendance
    {
        public int AttendenceId { get; set; }
        public string PeopleCode { get; set; }
        public int SectionId { get; set; }
        public string Status { get; set; }

        public int CalendarKey { get; set; }
      
    }
}
