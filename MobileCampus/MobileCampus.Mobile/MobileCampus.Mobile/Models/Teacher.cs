﻿namespace MobileCampus.Mobile.Models
{
    public class Teacher
    {
        public string Id { get; set; }
        public string PeopleId { get; set; }
        public int PersonId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public bool Privacy { get; set; }
    }
}
