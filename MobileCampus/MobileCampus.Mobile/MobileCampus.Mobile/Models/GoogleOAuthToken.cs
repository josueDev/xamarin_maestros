﻿namespace MobileCampus.Mobile.Models
{

    public class GoogleOAuthToken
    {
        public string TokenType { get; set; }
        public string AccessToken { get; set; }
    }

}
