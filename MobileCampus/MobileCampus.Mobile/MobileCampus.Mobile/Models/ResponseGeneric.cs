﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileCampus.Mobile.Models
{
    public class ResponseGeneric<T> : ReponseBase where T : new()
    {
        public T Data { get; set; }
        public ResponseGeneric(T data) : base()
        {
            Data = data;
        }

        public ResponseGeneric(bool success) : base(success)
        {
            if (!success)
            {
                Data = new T();
            }
        }

        public ResponseGeneric()
        {

        }
    }
}
