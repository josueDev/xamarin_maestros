﻿namespace MobileCampus.Mobile.Models
{
    public class ReponseBase
    {
        public bool Success { get; set; }
        public bool Technical { get; set; } = false;

        public string Message { get; set; }
        public string TechnicalMessage { get; set; }

        public ReponseBase()
        {
            Success = true;
            MessageDefault();
        }

        private void MessageDefault()
        {
            Message = "Ok";
        }

        public ReponseBase(bool success)
        {
            Success = success;

            if (!Success)
                Message = "Algo salió mal, por favor intenta más tarde.";
            else
                MessageDefault();
        }
    }
}
