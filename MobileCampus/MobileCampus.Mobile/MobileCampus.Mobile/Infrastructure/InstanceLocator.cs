﻿using MobileCampus.Mobile.ViewModel;

namespace MobileCampus.Mobile.Infrastructure
{
    public class InstanceLocator
    {
        public MainViewModel Main { get; set; }

        public InstanceLocator()
        {
            this.Main = new MainViewModel();
        }
    }
}
