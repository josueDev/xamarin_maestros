﻿using GoogleLogin.Models;
using MobileCampus.Mobile.Models;
using MobileCampus.Mobile.Persistence;
using MobileCampus.Mobile.ViewModel;
using MobileCampus.Mobile.Views;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MobileCampus.Mobile
{
    public partial class App : Application
    {
        public static NavigationPage Navigator { get; internal set; }
        public static MasterPage Master { get; internal set; }

        public App()
        {
            InitializeComponent();

            var token = JsonConvert.DeserializeObject<ProfileTeacherToken>(Settings.Token);
            if (token != null)
            {
                if (token.Token.Expiration > DateTime.Now)
                {
                    MainViewModel.GetInstance().GoogleProfile = JsonConvert.DeserializeObject<GoogleProfile>(Settings.UserEmail); ; ;
                    MainViewModel.GetInstance().ProfileTeacher = JsonConvert.DeserializeObject<ProfileTeacherToken>(Settings.Token); ;
                    MainViewModel.GetInstance().StudentList = new StudentListViewModel();

                    this.MainPage = new MasterPage();

                    if (!MainViewModel.GetInstance().ProfileTeacher.Profile.Privacy)
                        VerifyPrivacy();

                    return;
                }
            }


            MainViewModel.GetInstance().Login = new LoginViewModel();
            MainPage = new NavigationPage(new LoginPage());

            //var splashPage = new NavigationPage(new SplashPage());
            //MainPage = splashPage;
        }

        private void VerifyPrivacy()
        {
            Task.Run(() =>
            {
                Task.Delay(300);
                Device.BeginInvokeOnMainThread(() =>
                {
                    MainViewModel.GetInstance().NoticePrivacy = new NoticePrivacyViewModel();
                    (Application.Current.MainPage as MasterDetailPage).
                    Detail = new NavigationPage(new NoticePrivacyPage())
                    {
                        BarBackgroundColor = Color.FromHex("#024280"),

                    };
                });
            });
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
