﻿
using GalaSoft.MvvmLight.Command;
using MobileCampus.Mobile.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MobileCampus.Mobile.ViewModel
{
    public class SquadViewModel : BaseViewModel
    {
        private ObservableCollection<SquadItemViewModel> items;
        private bool toggleColor = false;

        public SquadViewModel()
        {
            toggleColor = false;
            loadItemsList();
        }

        private void loadItemsList()
        {
            var lst = new List<SquadItemViewModel>()
            {
                new SquadItemViewModel()
                {
                    Name = "CIUDAD AZTECA",
                    Adress = "Av. Central #10 Mz. 531 Lotes 1-4 Col. Ciudad Azteca 3era Sección C.P. 55120. Ecatepec de Morelos Estado de México.",
                    BackgroundColor = "#024482"
                },
                new SquadItemViewModel()
                {
                    Name = "CENTRO",
                    Adress = "Bucareli No. 107, esquina General Prim Col. Juárez",
                    BackgroundColor = "#033d75"
                },
                new SquadItemViewModel()
                {
                    Name = "CHALCO",
                    Adress = "Juárez #21 Esq. Alzate Col. Centro Chalco Estado de México.",
                    BackgroundColor = "#024482"
                },
                new SquadItemViewModel()
                {
                    Name = "CHAPULTEPEC",
                    Adress = "Av. Chapultepec 478, Roma Norte, C.P. 06700, Ciudad de México, CDMX",
                    BackgroundColor = "#033d75"
                },
                new SquadItemViewModel()
                {
                    Name = "CUAUTITLÁN",
                    Adress = "Calle Morelos s/n Col. Paseo de Santa María, Cuautitlán Estado de México.",
                    BackgroundColor = "#024482"
                },
                new SquadItemViewModel()
                {
                    Name = "IZTAPALAPA",
                    Adress = "Calzada Ermita Iztapalapa #4018 esquina con Eje 5 Sur Col. Paraje Zacatepec C.P. 09500 Iztapalapa.",
                    BackgroundColor = "#033d75"
                },
                new SquadItemViewModel()
                {
                    Name = "LEÓN",
                    Adress = "Pradera #910 Parque Manzanares León. Guanajuato.",
                    BackgroundColor = "#024482"
                },
                new SquadItemViewModel()
                {
                    Name = "NORTE",
                    Adress = "Acueducto #13 Col. San Pedro Zacatenco",
                    BackgroundColor = "#033d75"
                },
                new SquadItemViewModel()
                {
                    Name = "ORIENTE",
                    Adress = "Centro Tepozan Av. Tepozan #3 Los Reyes la Paz",
                    BackgroundColor = "#024482"
                },
                new SquadItemViewModel()
                {
                    Name = "SAN ANGEL",
                    Adress = "Av. Revolución #1836 Col. San Ángel Del. Álvaro Obregón C.P. 01000 México D.F.",
                    BackgroundColor = "#033d75"
                },
                new SquadItemViewModel()
                {
                    Name = "SUR1",
                    Adress = "Calz. de Tlalpan #1064 Col. Nativitas",
                    BackgroundColor = "#024482"
                },
                new SquadItemViewModel()
                {
                    Name = "SUR2",
                    Adress = "Calz. de Tlalpan #1148 Col. del Carmen",
                    BackgroundColor = "#033d75"
                },
                new SquadItemViewModel()
                {
                    Name = "TLÁHUAC",
                    Adress = "Av. Tláhuac #1789 C.P. 09900. Col. San Lorenzo Tezonco.",
                    BackgroundColor = "#024482"
                },
                new SquadItemViewModel()
                {
                    Name = "TLALNEPANT",
                    Adress = "Av. Vía Gustavo Baz #142 Col. Bellavista",
                    BackgroundColor = "#033d75"
                },
                new SquadItemViewModel()
                {
                    Name = "TLALPAN",
                    Adress = "Calz. Tlalpan #390 Col. Viaducto Piedad.",
                    BackgroundColor = "#024482"
                },
                new SquadItemViewModel()
                {
                    Name = "TOLUCA",
                    Adress = "Av. Nicolás Bravo #100 Col. Centro.",
                    BackgroundColor = "#033d75"
                },
                new SquadItemViewModel()
                {
                    Name = "TOREO",
                    Adress = "Av. Ingenieros Militares #97 Col. Lomas de Sotelo",
                    BackgroundColor = "#024482"
                },
                new SquadItemViewModel()
                {
                    Name = "VIADUCTO",
                    Adress = "Calz. de Tlalpan #538 Col. Moderna",
                    BackgroundColor = "#033d75"
                },
                new SquadItemViewModel()
                {
                    Name = "VÍA MORELOS",
                    Adress = "Av. Vía Morelos #237 Col. Cerro Gordo Santa Clara Ecatepec",
                    BackgroundColor = "#024482"
                },
                new SquadItemViewModel()
                {
                    Name = "VILLA DE CORTES",
                    Adress = "Calz. Tlalpan #934 Col. Nativitas C.P. 03500 Del. Benito Juárez México D.F.",
                    BackgroundColor = "#033d75"
                },
                new SquadItemViewModel()
                {
                    Name = "XOLA",
                    Adress = "Calz. de Tlalpan #705 Col. Álamos.",
                    BackgroundColor = "#024482"
                }
            };

            this.items = new ObservableCollection<SquadItemViewModel>(lst);

        }

        public ObservableCollection<SquadItemViewModel> Items
        {
            get => this.items;
            set => this.SetValue(ref this.items, value);
        }

        public ICommand LoginCommand => new RelayCommand(Login);
        public ICommand SquadMapCommand => new RelayCommand(SquadMap);

        private void Login()
        {
            Task.Run(() =>
            {
                Task.Delay(300);
                Device.BeginInvokeOnMainThread(() =>
                {
                    MainViewModel.GetInstance().Login = new LoginViewModel();
                    Application.Current.MainPage = new NavigationPage(new LoginPage());
                });
            });
        }

        private void SquadMap()
        {
            Task.Run(() =>
            {
                Task.Delay(300);
                Device.BeginInvokeOnMainThread(() =>
                {
                    MainViewModel.GetInstance().SquadMap = new SquadMapViewModel();
                    Application.Current.MainPage = new NavigationPage(new SquadMapPage())
                    { BarBackgroundColor = Color.FromHex("#024280") };
                });
            });
        }

        private string GetColorToggle()
        {
            var color = toggleColor ? "#024482" : "#033d75";
            toggleColor = !toggleColor;

            return color;
        }
    }
}
