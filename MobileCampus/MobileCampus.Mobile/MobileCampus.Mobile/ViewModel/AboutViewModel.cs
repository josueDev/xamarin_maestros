﻿using GalaSoft.MvvmLight.Command;
using MobileCampus.Mobile.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MobileCampus.Mobile.ViewModel
{
    public class AboutViewModel : BaseViewModel
    {
        public AboutViewModel()
        {

        }

        public ICommand LoginCommand => new RelayCommand(Login);
        public ICommand SquadCommand => new RelayCommand(Squad);

        private void Login()
        {
            Task.Run(() =>
            {
                Task.Delay(300);
                Device.BeginInvokeOnMainThread(() =>
                {
                    MainViewModel.GetInstance().Login = new LoginViewModel();
                    Application.Current.MainPage = new NavigationPage(new LoginPage());
                });
            });
        }

        private void Squad()
        {
            Task.Run(() =>
            {
                Task.Delay(300);
                Device.BeginInvokeOnMainThread(() =>
                {
                    MainViewModel.GetInstance().Squad = new SquadViewModel();
                    Application.Current.MainPage = new NavigationPage(new SquadPage())
                    { BarBackgroundColor = Color.FromHex("#024280") };
                });
            });
        }
    }
}
