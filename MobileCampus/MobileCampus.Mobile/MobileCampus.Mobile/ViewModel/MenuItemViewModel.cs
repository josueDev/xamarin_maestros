﻿using GalaSoft.MvvmLight.Command;
using MobileCampus.Mobile.Persistence;
using MobileCampus.Mobile.Views;
using Rg.Plugins.Popup.Services;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MobileCampus.Mobile.ViewModel
{
    public class MenuItemViewModel : BaseViewModel
    {
        private string icon;
        private bool isSelect;
        private string backgroundColor;

        public int Id { get; set; }
        public string Title { get; set; }
        public string PageName { get; set; }

        public string Icon
        {
            get => this.icon;
            set => this.SetValue(ref this.icon, value);
        }

        public string BackgroundColor
        {
            get => this.backgroundColor;
            set => this.SetValue(ref this.backgroundColor, value);
        }

        public bool IsSelect
        {
            get => this.isSelect;
            set => this.SetValue(ref this.isSelect, value);
        }

        public ICommand SelectMenuCommand => new RelayCommand(SelectMenu);

        private string getIconMenu(string menu)
        {
            var nameIcon = string.Empty;

            if (menu.Contains("lista"))
                nameIcon = "ic_lista_aqua.png";
            else if (menu.Contains("perfil"))
                nameIcon = "ic_mi_perfil_aqua.png";
            else if (menu.Contains("salir"))
                nameIcon = "ic_salir_aqua.png";

            return nameIcon;
        }

        private string getIconMenuSelect(string menu)
        {
            var nameIcon = string.Empty;

            if (menu.Contains("lista"))
                nameIcon = "ic_lista_azul.png";
            else if (menu.Contains("perfil"))
                nameIcon = "ic_mi_perfil_azul.png";
            else if (menu.Contains("salir"))
                nameIcon = "ic_salir_azul.png";

            return nameIcon;
        }

        private void ClearSelection()
        {
            foreach (var item in MainViewModel.GetInstance().Menus)
            {
                item.IsSelect = false;
                item.BackgroundColor = "Transparent";
                item.Icon = getIconMenu(item.icon);
            }
        }

        private async void SelectMenu()
        {
            App.Master.IsPresented = false;

            switch (this.PageName)
            {
                case "Profile":

                    await Task.Run(() =>
                    {
                        Task.Delay(300);
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            MainViewModel.GetInstance().Profile = new ProfileViewModel();

                            (Application.Current.MainPage as MasterDetailPage).
                            Detail = new NavigationPage(new ProfilePage())
                            {
                                BarBackgroundColor = Color.FromHex("#087772")
                            };

                        });
                    });

                    break;
                case "StudenList":

                    await Task.Run(() =>
                    {
                        Task.Delay(300);
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            MainViewModel.GetInstance().StudentList = new StudentListViewModel();
                            (Application.Current.MainPage as MasterDetailPage).
                            Detail = new NavigationPage(new StudentListPage())
                            {
                                BarBackgroundColor = Color.FromHex("#024280"),
                                
                            };
                        });
                    });

                    break;
                default:

                    if(!string.IsNullOrEmpty(Settings.ToleranceTime))
                        if (IsTimeAvailableInit())
                        {
                            MainViewModel.GetInstance().Popup = new PopupAlertViewModel("Error", "¡Lo sentimos!, no puedes cerrar sesión antes del tiempo de tolerancia. ", "DE ACUERDO");
                            await PopupNavigation.Instance.PushAsync(new PopupAlertPage());
                            return;
                        }

                    Settings.IsRemember = false;
                    Settings.UserEmail = string.Empty;
                    Settings.Token = string.Empty;
                    Settings.AssistenceIdTeacher = string.Empty;
                    Settings.AssistenceStudent = string.Empty;
                    Settings.ToleranceTime = string.Empty;

                    MainViewModel.GetInstance().Login = new LoginViewModel();
                    Application.Current.MainPage = new NavigationPage(new LoginPage());
                    break;
            }

            ClearSelection();

            IsSelect = true;
            Icon = getIconMenuSelect(Icon);
            BackgroundColor = "#1aaba4";
        }

        private bool IsTimeAvailableInit()
        {
            var di = DateTime.Now;
            var df = DateTime.Parse($"{DateTime.Now.ToString("yyyy/MM/dd")} {Settings.StartTime}.000");

            var diff = di - df;

            // return (diff.TotalMinutes > int.Parse(Settings.ToleranceTime)) ? false : true;
            return false;
        }

    }
}
