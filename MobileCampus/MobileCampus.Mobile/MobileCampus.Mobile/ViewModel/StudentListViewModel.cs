﻿using GalaSoft.MvvmLight.Command;
using MobileCampus.Mobile.Models;
using MobileCampus.Mobile.Persistence;
using MobileCampus.Mobile.Services;
using MobileCampus.Mobile.Views;
using Newtonsoft.Json;
using Plugin.LocalNotifications;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MobileCampus.Mobile.ViewModel
{
    public class StudentListViewModel : BaseViewModel
    {
        private readonly ApiService apiService;
        private List<Models.StudentList> LstStudent;
        private ObservableCollection<StudentItemViewModel> student;
        private string titleSubject;
        private bool isRefreshing;
        private bool toggleColor = false;
        private bool isEnabled = false;
        private int idSubject;
        private bool isAssistance = false;
        private bool isTimeStartAvailable = true;
        private string peopleIdTeacher;
        private float percentage = 0;
        private int calendarKey;
        private int toleranceTime = 0;

        public StudentListViewModel()
        {
            toggleColor = false;
            apiService = new ApiService();
            LoadInfoSubjects();
            IsEnabled = true;
        }


        public ObservableCollection<StudentItemViewModel> Student
        {
            get => this.student;
            set => this.SetValue(ref this.student, value);
        }

        public bool IsRefreshing
        {
            get => this.isRefreshing;
            set => this.SetValue(ref this.isRefreshing, value);
        }

        public string TitleSubject
        {
            get => this.titleSubject;
            set => this.SetValue(ref this.titleSubject, value);
        }

        public bool IsEnabled
        {
            get => this.isEnabled;
            set => this.SetValue(ref this.isEnabled, value);
        }

        public int IdSubject
        {
            get => this.idSubject;
            set => this.SetValue(ref this.idSubject, value);
        }

        public ICommand RegisterCommand => new RelayCommand(Register);

        private async void Register()
        {
            await Task.Run(async () =>
            {

                if (!IsTimeAvailableInit(Settings.StartTime))
                {
                    MainViewModel.GetInstance().Popup = new PopupAlertViewModel("Error", "¡Lo sentimos!, has excedido el tiempo de tolerancia para registrar asistencia, no fue posible enviar tu petición. ", "DE ACUERDO");
                    await PopupNavigation.Instance.PushAsync(new PopupAlertPage());
                    return;
                }

                if (!isAssistance)
                {
                    this.IsRefreshing = true;
                    this.IsEnabled = false;
                    try
                    {
                        // valida tiempo final de la clase
                        if (IsTimeAvailableEnd())
                        {
                            await RegisterAttendanceTeacher("PRESENT");
                            //if (percentage >= 90)
                            await RegisterAttendanceStudent();

                            CrossLocalNotifications.Current.Cancel(1);
                        }
                        else
                        {
                            MainViewModel.GetInstance().Popup = new PopupAlertViewModel("Error", "No se pudo registrar la asistencia, se termino el tiempo de la clase", "DE ACUERDO");
                            await PopupNavigation.Instance.PushAsync(new PopupAlertPage());
                        }

                    }
                    catch (Exception ex)
                    {
                        MainViewModel.GetInstance().Popup = new PopupAlertViewModel("Error", "No se pudo registrar la asistencia", "DE ACUERDO");
                        await PopupNavigation.Instance.PushAsync(new PopupAlertPage());
                    }
                    this.IsEnabled = true;
                    this.IsRefreshing = false;
                }
                else
                {
                    //MainViewModel.GetInstance().Popup = new PopupAlertViewModel("Error", "La asistencia fue aplicada ya no es posible editarla", "DE ACUERDO");
                    //await PopupNavigation.Instance.PushAsync(new PopupAlertPage());

                    // valida tiempo final de la clase
                    if (IsTimeAvailableEnd())
                    {
                        await UpdateAttendanceStudent();
                    }
                    else
                    {
                        MainViewModel.GetInstance().Popup = new PopupAlertViewModel("Error", "No se pudo registrar la asistencia, se termino el tiempo de la clase", "DE ACUERDO");
                        await PopupNavigation.Instance.PushAsync(new PopupAlertPage());
                    }

                }


            });
        }

        private async Task<bool> UpdateAttendanceStudent()
        {
            var lstAsistence = GetListStudentUpdate();

            if (!lstAsistence.Any())
            {
                MainViewModel.GetInstance().Popup = new PopupAlertViewModel("Error", "No se pudo actualizar la asistencia", "DE ACUERDO");
                await PopupNavigation.Instance.PushAsync(new PopupAlertPage());
                return false;
            }

            var response = await apiService.UpdateAssistenceStudent(Application.Current.Resources["UrlAPI"].ToString(),
                                                         "api/Student/StudentAttendanceUpdateAsync",
                                                         lstAsistence,
                                                         "bearer",
                                                         MainViewModel.GetInstance().ProfileTeacher.Token.Token);
            if (!response.Success)
            {
                MainViewModel.GetInstance().Popup = new PopupAlertViewModel("Error", "No se pudo actualizar la asistencia", "DE ACUERDO");
                await PopupNavigation.Instance.PushAsync(new PopupAlertPage());
            }
            else
            {
                this.isAssistance = true;
                MainViewModel.GetInstance().Popup = new PopupAlertViewModel("Succes", "La asistencia fue aplicada correctamente", "DE ACUERDO");
                await PopupNavigation.Instance.PushAsync(new PopupAlertPage());
            }
            return true;
        }

        private async Task<bool> RegisterAttendanceTeacher(string status)
        {
            var obj = new TeacherAttendance();
            obj.SectionId = IdSubject;
            obj.Status = status; // "AUTHENTICATED";
            obj.PeopleCode = peopleIdTeacher;
            obj.CalendarKey = calendarKey;

            var response = await apiService.RegisterAssistenceTeacher(Application.Current.Resources["UrlAPI"].ToString(),
                                                         "api/Teacher/TeacherAttendanceAsync",
                                                         obj,
                                                         "bearer",
                                                         MainViewModel.GetInstance().ProfileTeacher.Token.Token);
            if (!response.Success)
            {
                MainViewModel.GetInstance().Popup = new PopupAlertViewModel("Error", "No se pudo registrar la asistencia del docente", "DE ACUERDO");
                await PopupNavigation.Instance.PushAsync(new PopupAlertPage());
                return false;
            }

            Settings.AssistenceIdTeacher = response.Data.AssistenceId.ToString();

            return true;
        }

        private async Task<bool> RegisterAttendanceStudent()
        {
            var lstAsistence = GetListStudent();

            var response = await apiService.RegisterAssistenceStudent(Application.Current.Resources["UrlAPI"].ToString(),
                                                         "api/Student/StudentAttendanceAsync",
                                                         lstAsistence,
                                                         "bearer",
                                                         MainViewModel.GetInstance().ProfileTeacher.Token.Token);

            if (!response.Success)
            {
                MainViewModel.GetInstance().Popup = new PopupAlertViewModel("Error", "No se pudo registrar la asistencia", "DE ACUERDO");
                await PopupNavigation.Instance.PushAsync(new PopupAlertPage());
            }
            else
            {
                Settings.AssistenceStudent = JsonConvert.SerializeObject(response.Data);
                this.isAssistance = true;
                MainViewModel.GetInstance().Popup = new PopupAlertViewModel("Succes", "La asistencia fue aplicada correctamente", "DE ACUERDO");
                await PopupNavigation.Instance.PushAsync(new PopupAlertPage());
            }
            return true;
        }

        private List<StudentAttendance> GetListStudent()
        {
            var lstAsistence = new List<StudentAttendance>();

            foreach (var item in MainViewModel.GetInstance().StudentList.Student)
            {
                // para sp
                // 1.- falta
                // 2.- asitencia
                var findAsistence = 0;

                if (item.Asistence == 3)
                    findAsistence = 2;
                else
                    findAsistence = item.Asistence;

                lstAsistence.Add(new StudentAttendance
                {
                    AttendanceId = findAsistence,
                    PersonId = item.IdStudent,
                    SectionId = item.IdSubject
                });
            }
            //var j = lstAsistence.Count(l => l.AttendanceId == 2);
            //percentage = (float.Parse(lstAsistence.Count(l => l.AttendanceId == 2).ToString()) / float.Parse(lstAsistence.Count().ToString()) * 100);

            return lstAsistence;
        }

        private List<StudentAttendance> GetListStudentUpdate()
        {
            var lstAsistence = new List<StudentAttendance>();
            var lstAssistenceStudent = JsonConvert.DeserializeObject<List<ResponseAssistance>>(Settings.AssistenceStudent);

            if (lstAssistenceStudent != null)
            {
                foreach (var item in MainViewModel.GetInstance().StudentList.Student)
                {
                    // para sp
                    // 1.- falta
                    // 2.- asitencia
                    var findAsistence = 0;

                    if (item.Asistence == 3)
                        findAsistence = 2;
                    else
                        findAsistence = item.Asistence;

                    lstAsistence.Add(new StudentAttendance
                    {
                        AttendanceId = findAsistence,
                        PersonId = item.IdStudent,
                        IdAssistanceStudent = lstAssistenceStudent.FirstOrDefault(x => x.IdStudent == item.IdStudent).AssistenceId
                    });
                }
            }

            return lstAsistence;
        }


        private async void LoadInfoSubjects()
        {
            this.IsRefreshing = true;
            this.IsEnabled = false;
            var CalendarKeys = "";
            //TODO: cambiar valor 1
            var responseSubject = await apiService.GetSubjectTeacher(Application.Current.Resources["UrlAPI"].ToString(),
                                                             "api/Teacher/GetSubjectTeacher",
                                                             MainViewModel.GetInstance().ProfileTeacher.Profile.PersonId,
                                                             "bearer",
                                                             MainViewModel.GetInstance().ProfileTeacher.Token.Token);

            if (!responseSubject.Data.Any())
            {
                await AnyDataSubjectTeacher();
                this.IsRefreshing = false;
                return;
            }

            if (responseSubject.Success && responseSubject.Data[0].IdSubject > 0)
            {
                TitleSubject = responseSubject.Data[0].Subjects.ToUpper();
                IdSubject = responseSubject.Data[0].IdSubject;
                peopleIdTeacher = responseSubject.Data[0].IdPeople;
                calendarKey = responseSubject.Data[0].CalendarKey;
                SaveSettingSubjects(responseSubject.Data[0]);

                CalendarKeys = string.Join(",", responseSubject.Data.Select(c => c.CalendarKey).ToArray());

                var responseLstStudent = await apiService.GetStudentList(Application.Current.Resources["UrlAPI"].ToString(),
                                                                 "api/Student/GetStudentList",
                                                                 responseSubject.Data[0].IdTeacher,
                                                                 CalendarKeys, //responseSubject.Data[0].CalendarKey, //concatenar calendar keys
                                                                 "bearer",
                                                                 MainViewModel.GetInstance().ProfileTeacher.Token.Token);
                LstStudent = responseLstStudent.Data;

                if (LstStudent.Any())
                {
                    this.toleranceTime = LstStudent[0].ToleranceTime;
                    Settings.ToleranceTime = this.toleranceTime.ToString();
                }

                this.isTimeStartAvailable = IsTimeAvailableInit(responseSubject.Data[0].StartTime);

                this.isAssistance = LstStudent.Count(s => s.CodeAssistance == 1 || s.CodeAssistance == 2) > 0 ? true : false;


                RefreshStudentList();

                if (string.IsNullOrEmpty(Settings.AssistenceIdTeacher))
                {
                    await RegisterAttendanceTeacher("AUTHENTICATED");


                    var dateNotification = DateTime.Parse($"{DateTime.Now.ToString("yyyy/MM/dd")} {responseSubject.Data[0].StartTime}.000"); //DateTime.Now.AddMinutes((toleranceTime - 5));
                    dateNotification = dateNotification.AddMinutes((toleranceTime - 5));
                    //dateNotification = dateNotification.AddMinutes(5);

                    CrossLocalNotifications.Current.Show("Asistencia UIN", "¡Recuerda registrar asistencia!, el tiempo de tolerancia está por terminar.", 1, dateNotification);


                }


                this.IsEnabled = true;
            }
            else
            {
                await AnyDataSubjectTeacher();

            }

            this.IsRefreshing = false;
        }

        private async Task AnyDataSubjectTeacher()
        {
            this.IsEnabled = false;

            MainViewModel.GetInstance().Popup = new PopupAlertViewModel("Error", "No tiene materia disponible en este horario", "DE ACUERDO");
            await PopupNavigation.Instance.PushAsync(new PopupAlertPage());
        }

        private bool IsTimeAvailableInit(string timeInit)
        {
            var di = DateTime.Now;
            var df = DateTime.Parse($"{DateTime.Now.ToString("yyyy/MM/dd")} {timeInit}.000");

            var diff = di - df;

            return (diff.TotalMinutes > toleranceTime) ? false : true;
        }

        private bool IsTimeAvailableEnd()
        {
            var di = DateTime.Now;
            var df = DateTime.Parse($"{DateTime.Now.ToString("yyyy/MM/dd")} {Settings.EndTime}.000");

            var diff = di - df;

            return diff.TotalMinutes > 1 ? false : true;
        }

        private void SaveSettingSubjects(SubjectsTeacher subjectsTeacher)
        {

            if (!Settings.DateSubjects.Equals(DateTime.Now.ToString("dd-MM-yyyy")))
            {
                Settings.AssistenceIdTeacher = string.Empty;
                Settings.AssistenceStudent = string.Empty;
                Settings.ToleranceTime = string.Empty;
            }
            else if (Settings.DateSubjects.Equals(DateTime.Now.ToString("dd-MM-yyyy")))
            {
                var d1 = DateTime.Parse($"{DateTime.Now.ToString("yyyy/MM/dd")} {Settings.EndTime}.000");

                if (DateTime.Now.TimeOfDay > d1.TimeOfDay)
                {
                    Settings.AssistenceIdTeacher = string.Empty;
                    Settings.AssistenceStudent = string.Empty;
                    Settings.ToleranceTime = string.Empty;
                }
            }

            Settings.IdSubject = subjectsTeacher.IdSubject.ToString();
            Settings.IdTeacher = subjectsTeacher.IdSubject.ToString();
            Settings.StartTime = subjectsTeacher.StartTime;
            Settings.EndTime = subjectsTeacher.EndTime;
            Settings.DateSubjects = subjectsTeacher.DateSubjects;

        }

        private string GetColorToggle()
        {
            var color = toggleColor ? "#024482" : "#033d75";
            toggleColor = !toggleColor;

            return color;
        }

        // obtiene icono
        private string GetIcon(int assistance)
        {
            var icon = string.Empty;

            switch (assistance)
            {
                case 1:
                    icon = "ic_tache.png";
                    break;
                case 2:
                    icon = "ic_paloma.png";
                    break;
                default:
                    icon = "ic_tache.png";
                    break;
            }

            return icon;
        }

        // optiene codigo de asistencia
        private int CodeAssistance(int code)
        {
            if (code == 3) return 1;
            else return code;
        }

        // actualiza lista
        private void RefreshStudentList()
        {
            this.LstStudent = this.LstStudent.OrderBy(p => p.NameStudent).ToList();

            this.Student = new ObservableCollection<StudentItemViewModel>(
                this.LstStudent.Select(p => new StudentItemViewModel
                {
                    IdStudent = p.IdStuent,
                    Name = p.NameStudent,
                    Asistence = CodeAssistance(p.CodeAssistance),
                    Icon = GetIcon(p.CodeAssistance),
                    BackgroundColor = GetColorToggle(),
                    IdSubject = p.IdSubject
                })
            .ToList());
        }
    }
}
