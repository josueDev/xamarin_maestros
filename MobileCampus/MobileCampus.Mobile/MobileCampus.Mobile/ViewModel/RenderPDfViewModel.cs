﻿using System.IO;
using System.Reflection;

namespace MobileCampus.Mobile.ViewModel
{
    public class RenderPDfViewModel: BaseViewModel
    {
        private Stream pdfDocumentStream;
        public RenderPDfViewModel()
        {
            this.PdfDocumentStream = typeof(App).GetTypeInfo().Assembly.GetManifestResourceStream("MobileCampus.Mobile.File.AVISO_PRIVACIDAD_2019.pdf");
        }

        public Stream PdfDocumentStream
        {
            get => this.pdfDocumentStream;
            set => this.SetValue(ref this.pdfDocumentStream, value);
        }

    }
}
