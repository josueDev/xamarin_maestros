﻿using GalaSoft.MvvmLight.Command;
using GoogleLogin.Models;
using MobileCampus.Mobile.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace MobileCampus.Mobile.ViewModel
{
    public class MainViewModel : BaseViewModel
    {
        private static MainViewModel instance;
        public LoginViewModel Login { get; set; }
        public StudentListViewModel StudentList { get; set; }
        public ProfileViewModel Profile { get; set; }
        public RenderPDfViewModel RenderPdf { get; set; }
        public NoticePrivacyViewModel NoticePrivacy { get; set; }
        public AboutViewModel About { get; set; }

        public SquadViewModel Squad { get; set; }

        public SquadMapViewModel SquadMap { get; set; }

        public PopupAlertViewModel Popup { get; set; }

        public GoogleProfile GoogleProfile { get; set; }

        public ProfileTeacherToken ProfileTeacher { get; set; }


        public ObservableCollection<MenuItemViewModel> Menus { get; set; }

        public ICommand SelectCommandPrivacy => new RelayCommand(SelectPrivacy);

        private void SelectPrivacy()
        {
            App.Master.IsPresented = false;
            MainViewModel.GetInstance().RenderPdf = new RenderPDfViewModel();
            (Application.Current.MainPage as MasterDetailPage).
            Detail = new NavigationPage(new Views.RenderPdfPage())
            {
                BarBackgroundColor = Color.FromHex("#024280")
            };
        }

        public MainViewModel()
        {
            instance = this;
            this.LoadMenus();
        }

        public static MainViewModel GetInstance()
        {
            return instance == null ? new MainViewModel() : instance;
        }

        private void LoadMenus()
        {
            var menus = new List<Models.Menu>
            {
                new Models.Menu
                {
                    Id = 1,
                    Icon = "ic_lista_aqua",
                    PageName = "StudenList",
                    Title = "Lista de alumnos"
                },
                new Models.Menu
                {
                    Id = 2,
                    Icon = "ic_mi_perfil_aqua",
                    PageName = "Profile",
                    Title = "Mi Perfil"
                },

                new Models.Menu
                {
                    Id = 3,
                    Icon = "ic_salir_aqua",
                    PageName = "exit",
                    Title = "Salir"
                }
            };

            this.Menus = new ObservableCollection<MenuItemViewModel>(menus.Select(m => new MenuItemViewModel
            {
                Id = m.Id,
                Icon = m.Icon,
                PageName = m.PageName,
                Title = m.Title,
                BackgroundColor = "Transparent"
            }).ToList());
        }

    }
}
