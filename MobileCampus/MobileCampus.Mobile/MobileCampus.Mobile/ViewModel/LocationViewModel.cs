﻿using System;
using Xamarin.Forms.Maps;

namespace MobileCampus.Mobile.ViewModel
{
    public class LocationViewModel: BaseViewModel
    {
        public LocationViewModel()
        {
        }

        public string Address { get; set; }
        public string Description { get; set; }
        public Position Position { get; set; }
    }
}
