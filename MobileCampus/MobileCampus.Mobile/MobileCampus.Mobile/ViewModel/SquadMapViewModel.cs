﻿
using GalaSoft.MvvmLight.Command;
using MobileCampus.Mobile.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MobileCampus.Mobile.ViewModel
{
    public class SquadMapViewModel : BaseViewModel
    {
        public SquadMapViewModel()
        {
            loadLocation();
        }

        private void loadLocation()
        {
            var lst = new List<LocationViewModel>()
            {
                new LocationViewModel
                {
                    Address= "Av. Central #10 Mz. 531 Lotes 1-4 Col. Ciudad Azteca 3era Sección C.P. 55120. Ecatepec de Morelos Estado de México",
                    Description = "CIUDAD AZTECA",
                    Position = new Xamarin.Forms.Maps.Position(19.536788, -99.026135),
                },
                new LocationViewModel
                {
                    Address= "Av. Central #10 Mz. 531 Lotes 1-4 Col. Ciudad Azteca 3era Sección C.P. 55120. Ecatepec de Morelos Estado de México",
                    Description = "CENTRO",
                    Position = new Xamarin.Forms.Maps.Position(19.430231, -99.152286),
                },
                new LocationViewModel
                {
                    Address= "Juárez #21 Esq. Alzate Col. Centro Chalco Estado de México.",
                    Description = "CHALCO",
                    Position = new Xamarin.Forms.Maps.Position(19.259139, -98.897383),
                },
                new LocationViewModel
                {
                    Address= "Av. Central #10 Mz. 531 Lotes 1-4 Col. Ciudad Azteca 3era Sección C.P. 55120. Ecatepec de Morelos Estado de México",
                    Description = "CHAPULTEPEC",
                    Position = new Xamarin.Forms.Maps.Position(19.421729, -99.171170),
                },
                new LocationViewModel
                {
                    Address= "Juárez #21 Esq. Alzate Col. Centro Chalco Estado de México.",
                    Description = "COACALCO",
                    Position = new Xamarin.Forms.Maps.Position(19.624600, -99.076193),
                },
                new LocationViewModel
                {
                    Address= "Calle Morelos s/n Col. Paseo de Santa María, Cuautitlán Estado de México.",
                    Description = "CUAUTITLÁN",
                    Position = new Xamarin.Forms.Maps.Position(19.658605, -99.177576),
                },
                new LocationViewModel
                {
                    Address= "Calzada Ermita Iztapalapa #1693 Col. Octava Ampliación de San Miguel Iztapalapa C.P. 09830 Iztapalapa.",
                    Description = "ERMITA",
                    Position = new Xamarin.Forms.Maps.Position(19.352816, -99.078792),
                },
                new LocationViewModel
                {
                    Address= "Calzada Ermita Iztapalapa #4018 esquina con Eje 5 Sur Col. Paraje Zacatepec C.P. 09500 Iztapalapa.",
                    Description = "IZTAPALAPA",
                    Position = new Xamarin.Forms.Maps.Position(19.353092, -99.014957),
                },
                new LocationViewModel
                {
                    Address= "Pradera #910 Parque Manzanares León. Guanajuato.",
                    Description = "LEÓN",
                    Position = new Xamarin.Forms.Maps.Position(21.104448, -101.648522),
                },
                new LocationViewModel
                {
                    Address= "Acueducto #13 Col. San Pedro Zacatenco",
                    Description = "NORTE",
                    Position = new Xamarin.Forms.Maps.Position(19.502247, -99.117656),
                },
                new LocationViewModel
                {
                    Address= "Centro Tepozan Av. Tepozan #3 Los Reyes la Paz",
                    Description = "ORIENTE",
                    Position = new Xamarin.Forms.Maps.Position(19.364045, -98.989625),
                },
                new LocationViewModel
                {
                    Address= "Av. Revolución #1836 Col. San Ángel Del. Álvaro Obregón C.P. 01000 México D.F.",
                    Description = "SAN ANGEL",
                    Position = new Xamarin.Forms.Maps.Position(19.341889, -99.190640),
                },
                new LocationViewModel
                {
                    Address= "Calz. de Tlalpan #1064 Col. Nativitas",
                    Description = "SUR1",
                    Position = new Xamarin.Forms.Maps.Position(19.379506, -99.139817),
                },
                new LocationViewModel
                {
                    Address= "Calz. de Tlalpan #1148 Col. del Carmen",
                    Description = "SUR2",
                    Position = new Xamarin.Forms.Maps.Position(19.376887, -99.140174),
                },
                new LocationViewModel
                {
                    Address= "Av. Tláhuac #1789 C.P. 09900. Col. San Lorenzo Tezonco.",
                    Description = "TLÁHUAC",
                    Position = new Xamarin.Forms.Maps.Position(19.310226, -99.069561),
                },
                new LocationViewModel
                {
                    Address= "Av. Vía Gustavo Baz #142 Col. Bellavista",
                    Description = "TLALNEPANT",
                    Position = new Xamarin.Forms.Maps.Position(19.516974, -99.214201),
                },
                new LocationViewModel
                {
                    Address= "Calz. Tlalpan #390 Col. Viaducto Piedad.",
                    Description = "TLALPAN",
                    Position = new Xamarin.Forms.Maps.Position(19.402834, -99.136233),
                },
                new LocationViewModel
                {
                    Address= "Av. Nicolás Bravo #100 Col. Centro.",
                    Description = "TOLUCA",
                    Position = new Xamarin.Forms.Maps.Position(19.290969, -99.657358),
                },
                new LocationViewModel
                {
                    Address= "Av. Central #10 Mz. 531 Lotes 1-4 Col. Ciudad Azteca 3era Sección C.P. 55120. Ecatepec de Morelos Estado de México",
                    Description = "TOREO",
                    Position = new Xamarin.Forms.Maps.Position(19.454801, -99.216529),
                },new LocationViewModel
                {
                    Address= "Calz. de Tlalpan #538 Col. Moderna",
                    Description = "VIADUCTO",
                    Position = new Xamarin.Forms.Maps.Position(19.544950, -99.054516),
                },
                new LocationViewModel
                {
                    Address= "Av. Vía Morelos #237 Col. Cerro Gordo Santa Clara Ecatepec",
                    Description = "VÍA MORELOS",
                    Position = new Xamarin.Forms.Maps.Position(19.430231, -99.152286),
                },
                new LocationViewModel
                {
                    Address= "Calz. Tlalpan #934 Col. Nativitas C.P. 03500 Del. Benito Juárez México D.F.",
                    Description = "VILLA DE CORTES",
                    Position = new Xamarin.Forms.Maps.Position(19.384954, -99.138966),
                },
                new LocationViewModel
                {
                    Address= "Calz. de Tlalpan #705 Col. Álamos.",
                    Description = "XOLA",
                    Position = new Xamarin.Forms.Maps.Position(19.393924, -99.138492),
                },

            };

            this.Items = new ObservableCollection<LocationViewModel>(lst);
        }

        private ObservableCollection<LocationViewModel> items;

        public ICommand LoginCommand => new RelayCommand(Login);

        private void Login()
        {
            Task.Run(() =>
            {
                Task.Delay(300);
                Device.BeginInvokeOnMainThread(() =>
                {
                    MainViewModel.GetInstance().Login = new LoginViewModel();
                    Application.Current.MainPage = new NavigationPage(new LoginPage());
                });
            });
        }

        public ObservableCollection<LocationViewModel> Items
        {
            get => this.items;
            set => this.SetValue(ref this.items, value);
        }
    }
}
