﻿using GalaSoft.MvvmLight.Command;
using System.Windows.Input;

namespace MobileCampus.Mobile.ViewModel
{
    public class StudentItemViewModel: BaseViewModel
    {
        private int asistence;
        private string icon;

        public int IdStudent { get; set; }
        public string Name { get; set; }
        public string BackgroundColor { get; set; }
        public int IdSubject { get; set; }

        public StudentItemViewModel()
        {

        }

        public int Asistence
        {
            get => this.asistence;
            set => this.SetValue(ref this.asistence, value);
        }

        public string Icon
        {
            get => this.icon;
            set => this.SetValue(ref this.icon, value);
        }

        public ICommand SelectItemStudent => new RelayCommand<int>(this.SelectStudent);

        private string getIcon(int assistance)
        {
            var icon = string.Empty;

            switch (assistance)
            {
                case 1:
                    icon = "ic_tache.png";
                    break;
                case 2:
                    icon = "ic_paloma.png";
                    break;
                default:
                    icon = "ic_tache.png";
                    break;
            }

            return icon;
        }

        private void SelectStudent(int index)
        {
            if (asistence == 1)
            {
                Asistence = 2;
            }
            else if (asistence == 2)
            {
                Asistence = 1;
            }
            else
            {
                Asistence = 2;
            }
            Icon = getIcon(asistence); ;
        }
    }
}
