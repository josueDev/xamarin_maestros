﻿using GalaSoft.MvvmLight.Command;
using MobileCampus.Mobile.Contract;
using MobileCampus.Mobile.Models;
using MobileCampus.Mobile.Persistence;
using MobileCampus.Mobile.Services;
using MobileCampus.Mobile.Views;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MobileCampus.Mobile.ViewModel
{
    public class LoginViewModel : BaseViewModel, IGoogleAuthenticationDelegate
    {
        private readonly IGoogleAuthService _googleAuthService;
        private readonly ApiService apiService;
        private bool isEnabled;
        private bool isRunning;

        public LoginViewModel()
        {
            _googleAuthService = DependencyService.Resolve<IGoogleAuthService>();
            apiService = new ApiService();
            IsEnabled = true;
        }

        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsEnabled
        {
            get => this.isEnabled;
            set => this.SetValue(ref this.isEnabled, value);
        }

        public bool IsRunning
        {
            get => this.isRunning;
            set => this.SetValue(ref this.isRunning, value);
        }

        public ICommand LoginCommand => new RelayCommand(Login);

        public ICommand AboutCommand => new RelayCommand(About);

        private void About()
        {

            Task.Run(() =>
            {
                Task.Delay(300);
                Device.BeginInvokeOnMainThread(() =>
                {
                    MainViewModel.GetInstance().About = new AboutViewModel();
                    Application.Current.MainPage = new NavigationPage(new AboutPage())
                    { BarBackgroundColor = Color.FromHex("#024280") };
                });
            });
        }

        public void OnAuthenticationCanceled()
        {
            this.IsEnabled = true;
            this.IsRunning = false;
            return;
        }

        public async void OnAuthenticationCompleted(GoogleOAuthToken token)
        {
            this.IsEnabled = false;
            this.IsRunning = true;

            var googleService = new GoogleAccountInfoService();
            var objProfile = await googleService.GetProfileAsync(token.TokenType, token.AccessToken);

            if (objProfile.Email.Contains("@uinenlinea.mx"))
            {
                var isTeacher = await apiService.IsTeacherAsync(Application.Current.Resources["UrlAPI"].ToString(), "api/teacher/IsTeacher", objProfile.Email);

                if (!isTeacher.Success)
                {
                    MainViewModel.GetInstance().Popup = new PopupAlertViewModel("Error", "Ocurrió un problema al obtener el perfil del usuario.", "DE ACUERDO");
                    await PopupNavigation.Instance.PushAsync(new PopupAlertPage());
                }

                if (isTeacher.Data == true)
                {
                    var profileToken = await GetProfileTokenTecher(objProfile.Email);
                    if (profileToken != null)
                    {
                        MainViewModel.GetInstance().GoogleProfile = objProfile;
                        MainViewModel.GetInstance().ProfileTeacher = profileToken;
                        MainViewModel.GetInstance().StudentList = new StudentListViewModel();

                        Settings.IsRemember = true;
                        Settings.UserEmail = JsonConvert.SerializeObject(objProfile);
                        Settings.Token = JsonConvert.SerializeObject(profileToken);

                        Application.Current.MainPage = new MasterPage();

                        if (!profileToken.Profile.Privacy)
                            VerifyPrivacy();
                    }
                }
                else
                {
                    MainViewModel.GetInstance().Popup = new PopupAlertViewModel("Error", "Solo pueden acceder correos con el perfil de docente.", "DE ACUERDO");
                    await PopupNavigation.Instance.PushAsync(new PopupAlertPage());
                }


            }
            else
            {
                MainViewModel.GetInstance().Popup = new PopupAlertViewModel("Error", "Solo pueden acceder con un correo que tenga el domino de Universidad Insurgentes.", "DE ACUERDO");
                await PopupNavigation.Instance.PushAsync(new PopupAlertPage());
            }

            this.IsEnabled = true;
            this.IsRunning = false;
        }

        public async void OnAuthenticationFailed(string message, Exception exception)
        {
            MainViewModel.GetInstance().Popup = new PopupAlertViewModel("Error", $"Ocurrio un error al iniciar sesión {message}", "DE ACUERDO");
            await PopupNavigation.Instance.PushAsync(new PopupAlertPage());
            this.IsEnabled = true;
            this.IsRunning = false;
        }

        private void VerifyPrivacy()
        {
            Task.Run(() =>
            {
                Task.Delay(300);
                Device.BeginInvokeOnMainThread(() =>
                {
                    MainViewModel.GetInstance().NoticePrivacy = new NoticePrivacyViewModel();
                    (Application.Current.MainPage as MasterDetailPage).
                    Detail = new NavigationPage(new NoticePrivacyPage())
                    {
                        BarBackgroundColor = Color.FromHex("#024280"),

                    };
                });
            });

        }

        private async void Login()
        {
            await Task.Run(() =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    this.IsRunning = true;
                    if (IsEnabled)
                        _googleAuthService.Autheticate(this);
                    IsEnabled = false;

                });
            });
        }

        private async Task<ProfileTeacherToken> GetProfileTokenTecher(string email)
        {
            var result = await apiService.ProfileTokenTeacherAsync(Application.Current.Resources["UrlAPI"].ToString(), "api/teacher/GetProfile", email);

            if (!result.Success)
            {
                MainViewModel.GetInstance().Popup = new PopupAlertViewModel("Error", $"Ocurrió un error al obtener el perfil.", "DE ACUERDO");
                await PopupNavigation.Instance.PushAsync(new PopupAlertPage());
                return null;
            }

            return result.Data;
        }
    }
}
