﻿using GalaSoft.MvvmLight.Command;
using Rg.Plugins.Popup.Services;
using System;
using System.Windows.Input;

namespace MobileCampus.Mobile.ViewModel
{
    public class PopupAlertViewModel: BaseViewModel
    {
        private string message;
        private string typeAlert;
        private string messageButton;
        private string colorTextButton;

        public PopupAlertViewModel(string typeAlert, string message, string messageButton)
        {
            Message = message;
            MessageButton = messageButton;
            TypeAlert = setColorTypeAlert(typeAlert);
        }

  
        public string TypeAlert
        {
            get => this.typeAlert;
            set => this.SetValue(ref this.typeAlert, value);
        }

        public string Message
        {
            get => this.message;
            set => this.SetValue(ref this.message, value);
        }

        public string MessageButton
        {
            get => this.messageButton;
            set => this.SetValue(ref this.messageButton, value);
        }

        public string ColorTextButton
        {
            get => this.colorTextButton;
            set => this.SetValue(ref this.colorTextButton, value);
        }

        public ICommand ActionButtonCommand => new RelayCommand(ActionButton);

        private async void ActionButton()
        {
            await PopupNavigation.Instance.PopAsync(true);
        }

        private string setColorTypeAlert(string typeAlert)
        {
            string color = string.Empty;

            switch (typeAlert)
            {
                case "Succes":
                    color = "#1aaba4";
                    colorTextButton = "#ed008c";
                    break;
                case "Error":
                    color = "#ed008c";
                    colorTextButton = "#1aaba4";
                    break;
                default:
                    break;
            }

            return color;
        }
    }
}
