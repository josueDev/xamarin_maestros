﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileCampus.Mobile.ViewModel
{
    public class SquadItemViewModel : BaseViewModel
    {
        public string BackgroundColor { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
    }
}