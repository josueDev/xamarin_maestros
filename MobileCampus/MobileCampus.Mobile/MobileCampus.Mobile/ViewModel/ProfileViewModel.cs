﻿namespace MobileCampus.Mobile.ViewModel
{
    public class ProfileViewModel: BaseViewModel
    {
        private string name;
        private string peopleId;

        public ProfileViewModel()
        {
            Name = MainViewModel.GetInstance().ProfileTeacher.Profile.FullName;
            PeopleId = $"{MainViewModel.GetInstance().ProfileTeacher.Profile.PeopleId}";
        }

        public string Name
        {
            get => this.name;
            set => this.SetValue(ref this.name, value);
        }

        public string PeopleId
        {
            get => this.peopleId;
            set => this.SetValue(ref this.peopleId, value);
        }
    }
}
