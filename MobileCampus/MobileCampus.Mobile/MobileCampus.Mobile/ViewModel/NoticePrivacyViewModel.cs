﻿using GalaSoft.MvvmLight.Command;
using MobileCampus.Mobile.Models;
using MobileCampus.Mobile.Persistence;
using MobileCampus.Mobile.Services;
using MobileCampus.Mobile.Views;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MobileCampus.Mobile.ViewModel
{
    public class NoticePrivacyViewModel : BaseViewModel
    {
        private bool isChecked;
        private bool isRunning;
        private bool isEnabled;
        private readonly ApiService apiService;

        public NoticePrivacyViewModel()
        {
            apiService = new ApiService();
            
        }

        public bool IsChecked
        {
            get => this.isChecked;
            set => this.SetValue(ref this.isChecked, value);
        }

        public bool IsRunning
        {
            get => this.isRunning;
            set => this.SetValue(ref this.isRunning, value);
        }

        public bool IsEnabled
        {
            get => this.isEnabled;
            set => this.SetValue(ref this.isEnabled, value);
        }

        public ICommand RegisterPrivacyCommand => new RelayCommand(RegisterPrivacy);

        private async void RegisterPrivacy()
        {
            await Task.Run(() =>
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (IsChecked)
                    {
                        this.IsRunning = true;
                        this.IsEnabled = false;
                        var response = await UpdatePrivacy();

                        if (response)
                        {
                            MainViewModel.GetInstance().ProfileTeacher.Profile.Privacy = true;

                            Settings.Token = JsonConvert.SerializeObject(MainViewModel.GetInstance().ProfileTeacher);

                            await Task.Run(() =>
                            {
                                Task.Delay(300);
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    MainViewModel.GetInstance().StudentList = new StudentListViewModel();
                                    (Application.Current.MainPage as MasterDetailPage).
                                    Detail = new NavigationPage(new StudentListPage())
                                    {
                                        BarBackgroundColor = Color.FromHex("#024280"),

                                    };
                                });
                            });

                        }
                        else
                        {
                            MainViewModel.GetInstance().Popup = new PopupAlertViewModel("Error", "Ocurrió un error al aceptar los terminos y condiciones", "DE ACUERDO");
                            await PopupNavigation.Instance.PushAsync(new PopupAlertPage());
                        }

                        this.IsRunning = false;
                        this.IsEnabled = true;
                    }
                    else
                    {
                        MainViewModel.GetInstance().Popup = new PopupAlertViewModel("Error", "Debe aceptar los terminos y condiciones", "DE ACUERDO");
                        await PopupNavigation.Instance.PushAsync(new PopupAlertPage());
                    }
                    
                });
            });
        }

        private async Task<bool> UpdatePrivacy()
        {
            var result = await apiService.UpdateNoticePrivacyTeacher(
                                                                    Application.Current.Resources["UrlAPI"].ToString(),
                                                                    "api/teacher/TeacherUpdateNoticePrivacyAsync",
                                                                    new Teacher { Id = MainViewModel.GetInstance().ProfileTeacher.Profile.Id},
                                                                    "bearer",
                                                                    MainViewModel.GetInstance().ProfileTeacher.Token.Token);

            return result.Success;
        }
    }
}
