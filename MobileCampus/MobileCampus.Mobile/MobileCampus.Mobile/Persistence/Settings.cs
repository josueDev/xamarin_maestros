﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace MobileCampus.Mobile.Persistence
{
    public static class Settings
    {
        private const string token = "token";
        private const string userEmail = "userEmail";
        private const string userPassword = "userPassword";
        private const string isRemember = "isRemember";
        public const string assistenceIdTeacher = "";

        // para materia del docente
        private const string dateSubjects = "dateSubjects";
        private const string endTime = "endTime";
        private const string startTime = "startTime";
        private const string idSubject = "idSubject";
        private const string idTeacher = "idTeacher";
        private const string assistenceStudent = "assistenceStudent";
        private const string toleranceTime = "toleranceTime";

        private static readonly string stringDefault = string.Empty;
        private static readonly bool boolDefault = false;


        private static ISettings AppSettings => CrossSettings.Current;

        public static string Token
        {
            get => AppSettings.GetValueOrDefault(token, stringDefault);
            set => AppSettings.AddOrUpdateValue(token, value);
        }

        public static string UserEmail
        {
            get => AppSettings.GetValueOrDefault(userEmail, stringDefault);
            set => AppSettings.AddOrUpdateValue(userEmail, value);
        }

        public static string UserPassword
        {
            get => AppSettings.GetValueOrDefault(userPassword, stringDefault);
            set => AppSettings.AddOrUpdateValue(userPassword, value);
        }

        public static bool IsRemember
        {
            get => AppSettings.GetValueOrDefault(isRemember, boolDefault);
            set => AppSettings.AddOrUpdateValue(isRemember, value);
        }

        public static string DateSubjects
        {
            get => AppSettings.GetValueOrDefault(dateSubjects, stringDefault);
            set => AppSettings.AddOrUpdateValue(dateSubjects, value);
        }

        public static string EndTime
        {
            get => AppSettings.GetValueOrDefault(endTime, stringDefault);
            set => AppSettings.AddOrUpdateValue(endTime, value);
        }

        public static string StartTime
        {
            get => AppSettings.GetValueOrDefault(startTime, stringDefault);
            set => AppSettings.AddOrUpdateValue(startTime, value);
        }

        public static string IdSubject
        {
            get => AppSettings.GetValueOrDefault(idSubject, stringDefault);
            set => AppSettings.AddOrUpdateValue(idSubject, value);
        }

        public static string IdTeacher
        {
            get => AppSettings.GetValueOrDefault(idTeacher, stringDefault);
            set => AppSettings.AddOrUpdateValue(idTeacher, value);
        }

        public static string AssistenceIdTeacher
        {
            get => AppSettings.GetValueOrDefault(assistenceIdTeacher, stringDefault);
            set => AppSettings.AddOrUpdateValue(assistenceIdTeacher, value);
        }

        public static string AssistenceStudent
        {
            get => AppSettings.GetValueOrDefault(assistenceStudent, stringDefault);
            set => AppSettings.AddOrUpdateValue(assistenceStudent, value);
        }

        public static string ToleranceTime
        {
            get => AppSettings.GetValueOrDefault(toleranceTime, stringDefault);
            set => AppSettings.AddOrUpdateValue(toleranceTime, value);
        }
    }

}
