﻿using Android.App;
using Android.Runtime;
using MobileCampus.Mobile.CampusShiny;
using Shiny;
using System;

namespace MobileCampus.Mobile.Droid
{
    [Application]
    public class MainApplication: ShinyAndroidApplication<CampusShinyStartup>
    {
        public MainApplication(IntPtr handle, JniHandleOwnership transfer) : base(handle, transfer)
        {

        }
    }
}