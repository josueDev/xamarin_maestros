﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MobileCampus.Mobile.Droid
{
    //[Activity(Label = "Activity1")]
    //public class Activity1 : Activity
    //{
    //    protected override void OnCreate(Bundle savedInstanceState)
    //    {
    //        base.OnCreate(savedInstanceState);

    //        // Create your application here
    //    }

    //    protected override void OnResume()
    //    {
    //        base.OnResume();
    //        StartActivity(new Intent(Application.Context, typeof(MainActivity)));
    //    }
    //}

    [Activity(Theme = "@style/SplashTheme", MainLauncher = true, NoHistory = true)]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        protected override void OnResume()
        {
            base.OnResume();
            //System.Threading.Thread.Sleep(189);
            StartActivity(new Intent(Application.Context, typeof(MainActivity)));
        }
    }
}