﻿using MobileCampus.Mobile.Contract;
using MobileCampus.Mobile.Droid.Services;
using MobileCampus.Mobile.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(GoogleAuthService))]
namespace MobileCampus.Mobile.Droid.Services
{
    public class GoogleAuthService : IGoogleAuthService
    {
        internal static MainActivity MainActivity { get; set; }

        //com.companyname.mobilecampus.mobile:/oauth2redirect
        //com.uin.universidadsur.mobile:/oauth2redirect

        public void Autheticate(IGoogleAuthenticationDelegate googleAuthenticationDelegate)
        {
            GoogleAuthenticatorHelper.Auth = new GoogleAuthenticator(
               Application.Current.Resources["KeyAndroid"].ToString(),
               "email",
               "com.companyname.mobilecampus.mobile:/oauth2redirect",
               googleAuthenticationDelegate);

            // Display the activity handling the authentication
            var authenticator = GoogleAuthenticatorHelper.Auth.GetAuthenticator();
            var intent = authenticator.GetUI(MainActivity);
            MainActivity.StartActivity(intent);
           

            //AuthenticationState.Authenticator = authenticator;
		    //var presenter = new Xamarin.Auth.Presenters.OAuthLoginPresenter();
			//presenter.Login(authenticator);
        }
    }
}
