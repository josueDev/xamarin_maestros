﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MobileCampus.Common.ViewModel;
using MobileCampus.Data.Entity;
using MobileCampus.Service.Contract;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MobileCampus.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class TeacherController : ControllerBase
    {
        private readonly ITeacherService teacherService;
        private readonly IUserService userService;

        public TeacherController(ITeacherService teacherService, IUserService userService)
        {
            this.teacherService = teacherService;
            this.userService = userService;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("GetProfile")]
        public async Task<IActionResult> GetProfile([FromBody]EmailTeacherViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.Email))
                {
                    return BadRequest(new ResponseGenericViewModel<ProfileTokenTeacherViewModel>(false)
                    {
                        Message = "Debe proporcionar un correo"
                    });
                }

                var teacher = await teacherService.GetProfileAsync(model.Email);
                var token = await userService.CreateTokenAsync(model.Email);

                if (teacher == null)
                {
                    return BadRequest(new ResponseGenericViewModel<ProfileTokenTeacherViewModel>(false)
                    {
                        Message = "No se encontro el docente"
                    });
                }

                return Ok(new ResponseGenericViewModel<ProfileTokenTeacherViewModel>(true)
                {
                    Data = new ProfileTokenTeacherViewModel()
                    {
                        Profile = teacher,
                        Token = token
                    }
                });

            }
            catch (System.Exception ex)
            {
                return BadRequest(new ResponseGenericViewModel<ProfileTokenTeacherViewModel>(false)
                {
                    Message = $"ex: {ex.Message}",
                    Technical = true,
                    TechnicalMessage = ex.Message
                });
            }


        }

        [HttpGet]
        [AllowAnonymous]
        [Route("IsTeacher")]
        public async Task<IActionResult> IsTeacher(string email)
        {
            try
            {
                if (string.IsNullOrEmpty(email))
                {
                    return BadRequest(new ResponseGenericViewModel<ProfileTokenTeacherViewModel>(false)
                    {
                        Message = "Debe proporcionar un correo"
                    });
                }

                var isTeacher = await teacherService.IsTeacherAsync(email);

                return Ok(new ResponseGenericViewModel<bool>(true)
                {
                    Data = isTeacher
                });


            }
            catch (System.Exception ex)
            {
                return BadRequest(new ResponseGenericViewModel<bool>(false)
                {
                    Message = $"ex: {ex.Message}",
                    Technical = true,
                    TechnicalMessage = ex.Message
                });
            }
        }

        [HttpPost]
        [Route("GetSubjectTeacher")]
        public async Task<IActionResult> GetSubjectTeacher([FromBody]EmailTeacherViewModel model)
        {
            try
            {
                var subteacher = await teacherService.GetSubjectTeacherAsync(model.IdPersonTeacher);

                return Ok(new ResponseGenericViewModel<List<SubjectsTeacherViewModel>>(true)
                {
                    Data = subteacher
                });

            }
            catch (System.Exception ex)
            {
                return BadRequest(new ResponseGenericViewModel<List<SubjectsTeacherViewModel>>(false)
                {
                    Message = $"ex: {ex.Message}",
                    Technical = true,
                    TechnicalMessage = ex.Message
                });
            }
        }

        [HttpPost]
        [Route("TeacherAttendanceAsync")]
        public async Task<IActionResult> TeacherAttendanceAsync([FromBody]TeacherAttendanceViewModel model)
        {
            try
            {
                var subteacher = await teacherService.TeacherAttendanceAsync(model);

                return Ok(new ResponseGenericViewModel<TeacherAssistence>(subteacher)
                {
                    Data = subteacher
                });

            }
            catch (System.Exception ex)
            {
                return BadRequest(new ResponseGenericViewModel<TeacherAssistence>(false)
                {
                    Message = $"ex: {ex.Message}",
                    Technical = true,
                    TechnicalMessage = ex.Message
                });
            }
        }

        [HttpPost]
        [Route("TeacherAttendanceUpdateAsync")]
        public async Task<IActionResult> TeacherAttendanceUpdateAsync([FromBody]TeacherAttendanceViewModel model)
        {
            try
            {
                var subteacher = await teacherService.TeacherAttendanceUpdateAsync(model);

                return Ok(new ResponseGenericViewModel<bool>(subteacher)
                {
                    Data = subteacher
                });

            }
            catch (System.Exception ex)
            {
                return BadRequest(new ResponseGenericViewModel<bool>(false)
                {
                    Message = $"ex: {ex.Message}",
                    Technical = true,
                    TechnicalMessage = ex.Message
                });
            }
        }

        [HttpPost]
        [Route("TeacherUpdateNoticePrivacyAsync")]
        public async Task<IActionResult> TeacherUpdateNoticePrivacyAsync([FromBody]TeacherViewModel model)
        {
            try
            {
                var subteacher = await teacherService.TeacherUpdateNoticePrivacyAsync(model);

                return Ok(new ResponseGenericViewModel<bool>(subteacher)
                {
                    Data = subteacher
                });

            }
            catch (System.Exception ex)
            {
                return BadRequest(new ResponseGenericViewModel<bool>(false)
                {
                    Message = $"ex: {ex.Message}",
                    Technical = true,
                    TechnicalMessage = ex.Message
                });
            }
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("TestApi")]
        public IActionResult TestApi(string email)
        {
            try
            {
                return Ok(new ResponseGenericViewModel<bool>(true)
                {
                });
            }
            catch (System.Exception ex)
            {
                return BadRequest(new ResponseGenericViewModel<bool>(false)
                {
                    Message = $"ex: {ex.Message}",
                    Technical = true,
                    TechnicalMessage = ex.Message
                });
            }
        }

    }
}