﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MobileCampus.Common.ViewModel;
using MobileCampus.Data.Entity;
using MobileCampus.Service.Contract;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileCampus.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class StudentController : ControllerBase
    {
        private readonly IUserService userService;

        public StudentController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpPost]
        [Route("GetStudentList")]
        public async Task<IActionResult> GetStudentList([FromBody]EmailTeacherViewModel model)
        {
            try
            {              
                var lst = await userService.GetStudentListAsync(model.IdPersonTeacher, model.CalendarKeys);
                return Ok(new ResponseGenericViewModel<List<StudentListViewModel>>(true)
                {
                    Data = lst
                });

            }
            catch (System.Exception ex)
            {
                return BadRequest(new ResponseGenericViewModel<List<StudentListViewModel>>(false)
                {
                    Message = $"ex: {ex.Message}",
                    Technical = true,
                    TechnicalMessage = ex.Message
                });
            }
        }

        [HttpPost]
        [Route("StudentAttendanceAsync")]
        public async Task<IActionResult> StudentAttendanceAsync([FromBody]List<StudentAttendanceViewModel> model)
        {
            try
            {
                var response = await userService.StudentAttendanceAsync(model);

                if (!response.Any()) {
                    return Ok(new ResponseGenericViewModel<bool>(true)
                    {
                        Message = "No se guardaron las asistencias"
                    });
                }

                return Ok(new ResponseGenericViewModel<List<StudentAssistanceViewModel>>(true)
                {
                    Data = response
                });

            }
            catch (System.Exception ex)
            {
                return BadRequest(new ResponseGenericViewModel<List<StudentAssistance>>(false)
                {
                    Message = $"ex: {ex.Message}",
                    Technical = true,
                    TechnicalMessage = ex.Message
                });
            }
        }

        [HttpPost]
        [Route("StudentAttendanceUpdateAsync")]
        public async Task<IActionResult> StudentAttendanceUpdateAsync([FromBody]List<StudentAttendanceViewModel> model)
        {
            try
            {
                var response = await userService.StudentAttendanceUpdateAsync(model);

                if (!response.Any())
                {
                    return Ok(new ResponseGenericViewModel<bool>(true)
                    {
                        Message = "No se pudo actualizar las asistencias"
                    });
                }

                return Ok(new ResponseGenericViewModel<List<StudentAssistance>>(true)
                {
                    Data = response
                });

            }
            catch (System.Exception ex)
            {
                return BadRequest(new ResponseGenericViewModel<List<StudentAssistance>>(false)
                {
                    Message = $"ex: {ex.Message}",
                    Technical = true,
                    TechnicalMessage = ex.Message
                });
            }
        }

    }
}