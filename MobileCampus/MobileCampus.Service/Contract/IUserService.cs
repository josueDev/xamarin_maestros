﻿using MobileCampus.Common.ViewModel;
using MobileCampus.Data.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MobileCampus.Service.Contract
{
    public interface IUserService
    {
        Task<TokenViewModel> CreateTokenAsync(string email);       
        Task<List<StudentListViewModel>> GetStudentListAsync(int idPeopleTeacher, string CalendarKeys);
        Task<List<StudentAssistanceViewModel>> StudentAttendanceAsync(List<StudentAttendanceViewModel> model);
        Task<List<StudentAssistance>> StudentAttendanceUpdateAsync(List<StudentAttendanceViewModel> model);
    }
}
