﻿using MobileCampus.Common.ViewModel;
using MobileCampus.Data.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MobileCampus.Service.Contract
{
    public interface ITeacherService
    {
        Task<TeacherViewModel> GetProfileAsync(string email);
        Task<bool> IsTeacherAsync(string email);
        Task<List<SubjectsTeacherViewModel>> GetSubjectTeacherAsync(int idPersonTeacher);
        Task<TeacherAssistence> TeacherAttendanceAsync(TeacherAttendanceViewModel model);
        Task<bool> TeacherAttendanceUpdateAsync(TeacherAttendanceViewModel model);
        Task<bool> TeacherUpdateNoticePrivacyAsync(TeacherViewModel model);
    }
}
