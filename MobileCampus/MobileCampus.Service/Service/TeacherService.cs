﻿using MobileCampus.Common.ViewModel;
using MobileCampus.Data.Contract;
using MobileCampus.Data.Entity;
using MobileCampus.Service.Contract;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MobileCampus.Service.Service
{
    public class TeacherService : ITeacherService
    {
        private readonly ITeacherRepository teacherRepository;

        public TeacherService(ITeacherRepository teacherRepository)
        {
            this.teacherRepository = teacherRepository;
        }

        public async Task<TeacherViewModel> GetProfileAsync(string email)
        {
            try
            {
                var teacher = await teacherRepository.GetProfileAsync(email);
                var tViewModel = new TeacherViewModel();

                if (teacher == null)
                    return null;

                return new TeacherViewModel()
                {
                    Id = teacher.Id,
                    Email = teacher.Email,
                    FullName = teacher.FullName,
                    PeopleId = teacher.PeopleId,
                    PersonId = teacher.PersonId,
                    Privacy = teacher.Privacy
                };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<SubjectsTeacherViewModel>> GetSubjectTeacherAsync(int idPersonTeacher)
        {
            try
            {
                var lstobjViewModel = new List<SubjectsTeacherViewModel>();
                var obj = await teacherRepository.GetSubjectTeacherAsync(idPersonTeacher);

                if (obj !=null)
                {
                    foreach (var item in obj)
                    {
                        lstobjViewModel.Add(new SubjectsTeacherViewModel()
                        {
                            IdTeacher = item.IdTeacher,
                            IdPeople = item.IdPeople,
                            IdSubject = item.IdSubject,
                            Subjects = item.Subjects,
                            CalendarKey = item.CalendarKey,
                            EventKey = item.EventKey,
                            FullNameTeacher = item.FullNameTeacher,
                            StatusSubjects = item.StatusSubjects,
                            StartTime = item.StartTime,
                            EndTime = item.EndTime,
                            DateSubjects = item.DateSubjects

                        }
                        );
                    }
                }


                return lstobjViewModel;

                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> IsTeacherAsync(string email)
        {
            try
            {
                var teacher = await teacherRepository.GetProfileAsync(email);

                return teacher == null ? false : true;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<TeacherAssistence> TeacherAttendanceAsync(TeacherAttendanceViewModel model)
        {
            try
            {
                var response = await teacherRepository.TeacherAttendanceAsync(model.PeopleCode, model.SectionId, model.Status, model.CalendarKey);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> TeacherAttendanceUpdateAsync(TeacherAttendanceViewModel model)
        {
            try
            {
                var response = await teacherRepository.TeacherAttendanceUpdateAsync(model.AttendenceId, model.Status);

                return response > 0 ? true : false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> TeacherUpdateNoticePrivacyAsync(TeacherViewModel model)
        {
            try
            {
                var response = await teacherRepository.TeacherUpdateNoticePrivacyAsync(model.Id);

                return response > 0 ? true : false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
