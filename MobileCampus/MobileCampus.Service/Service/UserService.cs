﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MobileCampus.Common.ViewModel;
using MobileCampus.Data.Contract;
using MobileCampus.Data.Entity;
using MobileCampus.Service.Contract;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace MobileCampus.Service.Service
{
    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;
        private readonly ITeacherRepository teacherRepository;
        private readonly IConfiguration configuration;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;

        public UserService(IUserRepository userRepository,
            ITeacherRepository teacherRepository,
            IConfiguration configuration,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
        {
            this.userRepository = userRepository;
            this.teacherRepository = teacherRepository;
            this.configuration = configuration;
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        public async Task<TokenViewModel> CreateTokenAsync(string email)
        {
            try
            {
                await signInManager.SignOutAsync();
                var user = await teacherRepository.GetProfileAsync(email);
                var userIdentity = await userManager.FindByEmailAsync(user.Email);
                if (user == null)
                    return null;

                await signInManager.SignInAsync(userIdentity, isPersistent: false, authenticationMethod: null);
                return GenerateToken(user);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<List<StudentAssistanceViewModel>> StudentAttendanceAsync(List<StudentAttendanceViewModel> model)
        {
            var lstIdAssitence = new List<StudentAssistanceViewModel>();
            try
            {
                foreach (var item in model)
                {
                    var response = await userRepository.StudentAttendanceAsync(item.PersonId, item.AttendanceId, item.SectionId);
                    lstIdAssitence.Add(new StudentAssistanceViewModel { AssistenceId = response[0].AssistenceId, IdStudent = item.PersonId });
                }

                return lstIdAssitence;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<StudentAssistance>> StudentAttendanceUpdateAsync(List<StudentAttendanceViewModel> model)
        {
            var lstIdAssitence = new List<StudentAssistance>();
            try
            {
                foreach (var item in model)
                {
                    var response = await userRepository.StudentAttendanceUpdateAsync(item.IdAssistanceStudent, item.AttendanceId);
                    lstIdAssitence.Add(response[0]);
                }

                return lstIdAssitence;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        
        public async Task<List<StudentListViewModel>> GetStudentListAsync(int idPeopleTeacher, string CalendarKeys)
        {
            try
            {
                var lstStudent = await userRepository.GetStudentListAsync(idPeopleTeacher, CalendarKeys);

                var lstViewModel = new List<StudentListViewModel>();

                foreach (var item in lstStudent)
                {
                    lstViewModel.Add(new StudentListViewModel
                    {
                        IdStuent = item.IdStuent,
                        NameStudent = item.NameStudent,
                        CodeAssistance = item.CodeAssistance,
                        IdSubject = item.IdSubject,
                        CalendarKey = item.CalendarKey,
                        EventKey = item.EventKey,
                        ToleranceTime = item.tiempo_tolerancia

                    });
                }

                return lstViewModel;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        private TokenViewModel GenerateToken(Teacher user)
        {
            try
            {
                var claims = new[]
                                {
                            new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.configuration["Tokens:key"]));
                var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                var token = new JwtSecurityToken
                    (
                        this.configuration["Tokens:Issuer"],
                        this.configuration["Tokens:Audience"],
                        claims,
                        expires: DateTime.UtcNow.AddDays(15),
                        signingCredentials: credentials
                    );
                return
                new TokenViewModel
                {
                    Token = new JwtSecurityTokenHandler().WriteToken(token),
                    Expiration = token.ValidTo
                };
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}
